### Oblig2_inf101  - Lars J. Sverkeli

# Dette er et administrasjonsverktør først og fremst tiltenkt barneskolelærere.

Gjennom dette verktøyet kan man administrere klasser og trinn.
Alt styres gjennom et brukegrensersnitt bestående av knapper, drop-menyer og tekstbokser.

## Hvordan kjøre programmet?

Programmet ligger som en komprimert .zip-fil. 

For å åpne programmet må du først pakke ut innholdet i zip-filen og åpne mappen Klasseadministrator. Deretter finner du filen klasseadministrator.jar, som brukes for å kjøre programmet.

Hvordan man kjører programmet er avhengig av operativsystemet ditt. Dersom du bruker windows kan du kjøre klasseadministrator.bat. For andre operativsystemer kan du kjøre klasseadministrator.jar.

Dette programmet krever java versjon 17 for å kjøre. Dersom programmet ikke kjører må du oppdatere til nyeste versjon av java.



## Hvordan åpne backup?
For å åpne en tidligere versjon kan du åpne mappen "backup" som ligger i Klasseadministrator. Der finner du alle tilgjengelige backupfiler, sortert etter dato.
Backup-filene har formatet ÅÅÅÅ-MM-DD:HHmmSS

For å åpne en backup må du flytte kopiere backup-filen du ønsker å åpne til hovedmappen, slette filen "data.ser" og endre navn på backupfilen til "data.ser"

# Informasjon om programmet
## Overordnet
Når programmet kjøres åpnes hovedvinduet for administrasjon. Her kan man legge til, åpne og slette individuelle elever og klasser. Man kan også her legge inn overordnede notater og generere epostliste for alle elever som er lagt inn i programmet.
Dette er det eneste stedet man kan slette trinn, klasser eller elever.

For å legge til et trinn trykker man "nytt trinn" og skriver hvilket trinn som skal legges inn. Tilsvarende gjør man for klasser og elever. Man må legge inn trinn før man kan legge til klasser, og klasser før man kan legge til elever.

Minimuminformasjon som kreves for å legge inn en elev er fornavn for eleven. Dette kan endres senere. Alle elever som legges til må være tilknyttet en klasse, og all elevinformasjon kan endres senere.


## Trinn
På trinnivå finnes verktøy for å generere epostliste for hele trinnet og et verktøy for å generere grupper på tvers av klasser. Man kan også skrive notater på trinnivå.


## Klasse
Klassenivået er pr nå det mest utviklede nivået. Klassenivået viser en liste over alle elever i klassen, og ved å trykke på eleven får man opp nøkkelinformasjon om denne.
På klassenivå er det også verktøy for fraværsregistrering og lekseregistrering. Man kan også legge til notat både for enkeltelever og på klassenivå.

For å opprette en ny lekse, trykk opprett lekse og skriv inn hvilket fag og hvilken uke leksen tilhører. 
Man kan senere registrere kommentarer på hver enkelt elev for den aktuelle leksen ved å trykke "registrer lekse". Man vil da få en dropmeny der man velger leksen man vil registrere, og man vil deretter få en dialogboks med mulighet til å skrive inn individuelle kommentarer for hver elev for den aktuelle leksen.
Ved å trykke "Se lekseoversikt" får man en oversikt over alle lekser som er opprettet, med tilhørende kommentarer for hver individuelle elev formatert som et rutenett.

På samme måte som med lekser kan man generere fraværslister.
For å registrere fravær trykker man "registrer fravær". Man vil da få opp en dialogboks der man kan registrere status for den individuelle elev. Om man lar en tekstboks for en elev stå tom merkes denne med "ok".
Når man klikker "OK" på dialogboksen registreres fravær og merkes automatisk med dagens dato. Om man senere ønsker å redigere fraværslisten kan man trykke "rediger fravær" og velge datoen man vil redigere.

På samme måte som med lekser, kan man få en oversikt over all registrert fravær ved å trykke "se fraværsliste".

Lekseoversikt og fraværsliste er fargekodet. Feltet blir grønt for "ok", gult for "s" og rødt for "x"

Både fraværsoversikt og lekseoversikt kan kopieres til utklippstavle ved å klikke "kopier" ved siden av knappen for å åpne oversikten. 
Hele oversikten vil da legges i utklippstavlen og kan limes direkte inn i regneark, for eksempel excel.

For å lage grupper for klassen trykker man på "lag grupper" og skriver inn ønsket gruppestørrelse. Om det er elever som ikke kan være i samme gruppe, kan dette tas hensyn til ved å velge en elev og trykke "legg til inkompatibel elev". Man vil da kunne velge en elev i klassen fra en dropliste, og disse elevene vil ikke plasseres i samme gruppe.

Legg merke til at bortsett fra å ta hensyn til inkompatible elever er gruppegeneratoren basert på randomisering, og tar ikke hensyn til noen andre egenskaper ved elevene. Dersom veldig mange elever legges til som inkompatible med hverandre kan det være at gruppegeneratoren ikke klarer å lage grupper.


## Elev
Dersom man velger en elev og trykker "åpne elev" får man opp funksjonalitet til den individuelle eleven. Her kan man redigere elevinformasjon, legge til eller redigere foresatte, legge til eller fjerne inkompatible elever og skrive notat både for elev og foresatte.


### Kontakt
Dersom det er spørsmål om hvordan klasseadministratoren brukes, eller innspill til funksjonalitet som bør legges til kan utvikler kontaktes ved å sende mail til:

lars.sverkeli@gmail.com

En videodemonstrasjon av programmet finnes her:
https://youtu.be/SPeh4nWKGiQ (5 min)

