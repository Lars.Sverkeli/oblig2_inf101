package administrator.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NotatTest {

    @Test
    void notatTest(){
        LocalDateTime dato = LocalDateTime.now();
        Notat testNotat = new Notat("test");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH.mm");
        assertEquals("Dato: " + dato.format(format) + "\n" + "test" + "\n", testNotat.toString());
    }
}
