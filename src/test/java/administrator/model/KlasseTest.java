package administrator.model;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Group maker is tested in HelpersTest
 */
public class KlasseTest {

    @Test
    void testAddAndSortElever() {
        Trinn sjette = new Trinn("6");
        Klasse seksA = new Klasse(sjette, "a");
        assertEquals("Klasse 6A",seksA.toString());
        String[] elevData = {"Lars", "Sverkeli", "16.05.1989", "", ""};
        seksA.addElev(elevData);
        elevData[0] = "Hege";
        elevData[1] = "Støfring";
        seksA.addElev(elevData);

        assertEquals("Lars Sverkeli, 6A", seksA.getElev(0).toString());
        assertEquals("Hege Støfring, 6A", seksA.getElev(1).toString());

        seksA.sortElever();

        assertEquals("Hege Støfring, 6A", seksA.getElev(0).toString());
        assertEquals("Lars Sverkeli, 6A", seksA.getElev(1).toString());
    }

    @Test
    void testLekser() {
        Trinn sjette = new Trinn("6");
        Klasse seksA = new Klasse(sjette, "a");
        assertEquals("Klasse 6A",seksA.toString());
        String[] elevData = {"Lars", "Sverkeli", "16.05.1989", "", ""};
        seksA.addElev(elevData);
        elevData[0] = "Hege";
        elevData[1] = "Støfring";
        seksA.addElev(elevData);

        Elev elev1 = seksA.getElev(0);
        Elev elev2 = seksA.getElev(1);

        seksA.nyLekse("lekse");
        assertEquals(1, seksA.getLekser().size());
        assertEquals(1, elev1.getLekser().size());
        assertEquals(1, elev2.getLekser().size());

        seksA.nyLekse("lekse2");
        assertEquals(2, seksA.getLekser().size());
        assertEquals("lekse2", seksA.getLekser().get(0));
        assertEquals("lekse", seksA.getLekser().get(1));
        assertEquals(2, elev1.getLekser().size());
        assertEquals("", elev1.getLekse(0));
        assertEquals("", elev1.getLekse(1));
        assertEquals(2, elev2.getLekser().size());
        assertEquals("", elev2.getLekse(0));
        assertEquals("", elev2.getLekse(1));

        ArrayList<String> lekseData = new ArrayList<>();
        lekseData.add("a");
        lekseData.add("b");

        seksA.registrerLekse(lekseData, 1);

        assertEquals("lekse2", seksA.getLekser().get(0));
        assertEquals("lekse", seksA.getLekser().get(1));
        assertEquals("", elev1.getLekse(0));
        assertEquals("a", elev1.getLekse(1));
        assertEquals("", elev2.getLekse(0));
        assertEquals("b", elev2.getLekse(1));

        ArrayList<ArrayList<String>> lekseListe = seksA.getLekseListe();
        assertEquals("", lekseListe.get(0).get(0));
        assertEquals("a", lekseListe.get(0).get(1));
        assertEquals("", lekseListe.get(1).get(0));
        assertEquals("b", lekseListe.get(1).get(1));

        assertEquals("ab", Helpers.arrayListAsSingleString(seksA.getSingleLekse(1), ""));
    }

    @Test
    void testTilstede(){
        Trinn sjette = new Trinn("6");
        Klasse seksA = new Klasse(sjette, "a");
        assertEquals("Klasse 6A",seksA.toString());
        String[] elevData = {"Lars", "Sverkeli", "16.05.1989", "", ""};
        seksA.addElev(elevData);
        elevData[0] = "Hege";
        elevData[1] = "Støfring";
        seksA.addElev(elevData);

        Elev elev1 = seksA.getElev(0);
        Elev elev2 = seksA.getElev(1);

        String date = Helpers.getDateAsString();

        ArrayList<String> registrering = new ArrayList<>();
        registrering.add("");
        registrering.add("");

        seksA.registrerTilstede(date, registrering);
        assertEquals(1, seksA.getTilstede().size());
        assertEquals(1, elev1.getTilstedeListe().size());
        assertEquals(1, elev2.getTilstedeListe().size());

        registrering.set(0,"x");
        seksA.registrerTilstede(date, registrering);

        assertEquals(2, seksA.getTilstede().size());
        assertEquals(date, seksA.getTilstede().get(0));
        assertEquals(date, seksA.getTilstede().get(1));
        assertEquals(2, elev1.getTilstedeListe().size());
        assertEquals("x", elev1.getTilstedeListe().get(0));
        assertEquals("ok", elev1.getTilstedeListe().get(1));
        assertEquals(2, elev2.getTilstedeListe().size());
        assertEquals("ok", elev2.getTilstedeListe().get(0));
        assertEquals("ok", elev2.getTilstedeListe().get(1));

        registrering.set(0,"test");
        registrering.set(1,"test");
        seksA.setSingleTilstede(1, registrering);
        assertEquals("x", elev1.getTilstedeListe().get(0));
        assertEquals("test", elev1.getTilstedeListe().get(1));
        assertEquals("ok", elev2.getTilstedeListe().get(0));
        assertEquals("test", elev2.getTilstedeListe().get(1));

        assertEquals("testtest", Helpers.arrayListAsSingleString(seksA.getSingleTilstede(1), ""));

        ArrayList<ArrayList<String>> tilstedeListe = seksA.getTilstedeOversikt();
        assertEquals("x", tilstedeListe.get(0).get(0));
        assertEquals("test", tilstedeListe.get(0).get(1));
        assertEquals("ok", tilstedeListe.get(1).get(0));
        assertEquals("test", tilstedeListe.get(1).get(1));
    }
}
