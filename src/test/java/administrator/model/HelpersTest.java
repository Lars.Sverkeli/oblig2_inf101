package administrator.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;

public class HelpersTest {

    @Test
    void combineStringTest(){
        String[] list = {"a", "b", "c"};
        String combinedList = Helpers.stringArrayToSingleString(list, "\n");
        assertEquals("a\nb\nc\n", combinedList);

        combinedList = Helpers.stringArrayToSingleString(list, " ");
        assertEquals("a b c ", combinedList);
    }

    @Test
    void asStringArrayTest(){
        ArrayList<Elev> elevList = new ArrayList<>();
        Elev test = new Elev("Test", "test", "16051989", "", "", new Klasse((new Trinn("6")), "b"));
        for (int i = 0; i < 5; i++) {
            elevList.add(test);
        }

        String[] testArray = Helpers.asStringArray(elevList);
        String expectedString = test.toString();

        assertEquals(expectedString, testArray[1]);
        assertFalse(expectedString.equals(elevList.get(1)));
    }

    @Test
    void makeGruppeTest(){
        ArrayList<IElevReader> elever = new ArrayList<>();
        String bursdag = "16.05.1989";
        Trinn trinn = new Trinn("1");
        Klasse klasse = new Klasse(trinn, "b");
        for (int i = 0; i < 50; i++) {
            elever.add((IElevReader) new Elev(Integer.toString(i),Integer.toString(i), bursdag, "", "",klasse));
        }

        ArrayList<ArrayList<IElevReader>> groupMakerTest = Helpers.makeElevGroups(elever,10);

        assertEquals(5, groupMakerTest.size());
        assertEquals(10, groupMakerTest.get(0).size());

        // Look at printout, no elements should be repeated
        String groupsAsString = Helpers.nestedArrayListAsString(groupMakerTest, "\n", "\n");
        System.out.println(groupsAsString);


        // Test whether exclusion works
        Elev test1 = new Elev("1", "1", bursdag, "", "", klasse);
        Elev test2 = new Elev("2", "2", bursdag, "", "", klasse);
        Elev test3 = new Elev("3", "3", bursdag, "", "", klasse);
        Elev test4 = new Elev("4", "4", bursdag, "", "", klasse);

        test1.addNewNegativ(test2);

        elever = new ArrayList<>();
        elever.add(test1);
        elever.add(test2);

        assertEquals(null, Helpers.makeElevGroups(elever,2));

        elever.add(test3);
        elever.add(test4);

        // See that 1 and 2 are never on the same group
        for (int i = 0; i < 6; i++) {
            ArrayList<ArrayList<IElevReader>> groups = Helpers.makeElevGroups(elever, 2);
            groupsAsString = Helpers.nestedArrayListAsString(groups, "\n", "\n");
            System.out.println(groupsAsString);
        }

        test1.addNewNegativ(test3);
        test1.addNewNegativ(test4);

        assertEquals(null, Helpers.makeElevGroups(elever,2));

    }
}
