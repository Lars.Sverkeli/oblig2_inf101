package administrator.model;


import administrator.GUI.GUIHelpers;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrinnTest {

    @Test
    void clipboardTest(){
        GUIHelpers.addToClipboard("TestTestt");
    }

    @Test
    void trinnAddKlasseAndElevTest(){
        Trinn testTrinn = new Trinn("5");

        assertEquals("5. trinn", testTrinn.toString());

        testTrinn.addKlasse("B");
        assertEquals("Klasse 5B", testTrinn.getKlasser().get(0).toString());

        testTrinn.addKlasse("c");
        String[] elevData = {"Lars","Sverkeli", "16.05.1989", "", ""};
        testTrinn.getKlasser().get(0).addElev(elevData);
        elevData[0] = "Hege";
        elevData[1] = "Støfring";
        testTrinn.getKlasser().get(1).addElev(elevData);

        Elev elev = testTrinn.getElever().get(0);
        assertEquals("Lars Sverkeli, 5B", elev.toString());

        elev = testTrinn.getElever().get(1);
        assertEquals("Hege Støfring, 5C", elev.toString());
    }
}
