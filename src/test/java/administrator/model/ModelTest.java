package administrator.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ModelTest {

    @Test
    void testAddSortAndRemove() {
        AdministratorModel model = new AdministratorModel();
        model.addTrinn("1");
        model.addTrinn("3");
        model.addTrinn("2");

        assertEquals(3, model.getTrinnListe().size());
        assertEquals("1. trinn", model.getTrinnListe().get(0).toString());
        assertEquals("3. trinn", model.getTrinnListe().get(1).toString());

        model.sortTrinn();

        assertEquals("1. trinn", model.getTrinnListe().get(0).toString());
        assertEquals("2. trinn", model.getTrinnListe().get(1).toString());

        Trinn trinn1 = model.getTrinnListe().get(0);
        Trinn trinn2 = model.getTrinnListe().get(1);

        trinn2.addKlasse("a");
        trinn1.addKlasse("b");

        assertEquals("Klasse 1B", model.getKlasser().get(0).toString());
        assertEquals(2, model.getKlasser().size());

        Klasse klasse1 = model.getKlasser().get(0);
        assertEquals("Klasse 1B", klasse1.toString());

        Klasse klasse2 = model.getKlasser().get(1);
        assertEquals("Klasse 2A", klasse2.toString());

        String[] elevData = {"a", "", "", "", ""};
        for (int i = 0; i < 5; i++) {
            klasse1.addElev(elevData);
        }
        for (int i = 0; i < 5; i++) {
            klasse2.addElev(elevData);
        }

        assertEquals(10, model.getElever().size());

        model.removeElev(8);

        assertEquals(2, model.getKlasser().size());
        assertEquals(3, model.getTrinnListe().size());
        assertEquals(5, klasse1.getElever().size());
        assertEquals(4, klasse2.getElever().size());
        assertEquals(5, trinn1.getElever().size());
        assertEquals(4, trinn2.getElever().size());
        assertEquals(9, model.getElever().size());

        model.removeTrinn(1);

        assertEquals(1, model.getKlasser().size());
        assertEquals(2, model.getTrinnListe().size());
        assertEquals(5, model.getElever().size());

        model.removeTrinn(0);

        assertEquals(0, model.getKlasser().size());
        assertEquals(1, model.getTrinnListe().size());
        assertEquals(0, model.getElever().size());

    }
}
