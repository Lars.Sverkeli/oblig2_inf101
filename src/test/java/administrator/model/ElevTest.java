package administrator.model;

import org.junit.jupiter.api.Test;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ElevTest {


    /**
     * Functionality for Lekse and Tilstede is tested in KlasseTest
     */
    @Test
    void testTest(){
    }

    @Test
    void testMakeElev() {
        LocalDate date = LocalDate.now();
        Elev testElev = new Elev("Lars", "Sverkeli", "16.05.1989", "", "", new Klasse(new Trinn("3"), "a"));
        assertEquals("Lars Sverkeli, 3A",testElev.toString());
    }

    @Test
    void testAddGetNotat() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH.mm");
        LocalDateTime date = LocalDateTime.now();
        String dateString = date.format(format);
        Elev testElev = new Elev("Lars", "Sverkeli", "16.05.1989", "", "", new Klasse(new Trinn("3"), "A"));
        String notat = "test";
        testElev.addNotat(notat);

        String expected = "Dato: " + dateString + "\n" + notat + "\n";
        String actual = testElev.getNotater().get(0).toString();

        assertEquals(expected, actual);
    }

    @Test
    void testAddAndRemoveNegativ() {
        Elev test1 = new Elev("Lars", "Sverkeli",  "16.05.1989", "", "", new Klasse(new Trinn("2"), "a"));
        Elev test2 = new Elev("Hege", "Støfring", "16.05.1989", "", "", new Klasse(new Trinn("2"), "a"));

        test1.addNewNegativ(test2);

        assertEquals(test1.toString(),test2.getNegative()[0]);
        assertEquals(test2.toString(),test1.getNegative()[0]);
        assertTrue(test1.toString().equals(test2.getNegative()[0]));

        test1.removeNegativ(test2);
        assertTrue(0 == test1.getNegative().length);
        assertTrue(0 == test2.getNegative().length);
    }

    @Test
    void testGetDataAndRedigerElev() {
        LocalDate date = LocalDate.now();
        Elev testElev = new Elev("Lars", "Sverkeli", "16.05.1989", "92345612", "", new Klasse(new Trinn("3"), "a"));
        assertEquals("Lars Sverkeli, 3A", testElev.toString());
        String[] newData = testElev.getElevData();
        assertEquals(newData[0], "Lars");
        assertEquals(newData[1], "Sverkeli");
        assertEquals(newData[2],"16.05.1989");
        assertEquals(newData[3],"92345612");
        assertEquals(newData[4],"");

        newData[0] = "Hege";
        newData[1] = "Støfring";
        newData[2] = "12.04.1684";

        testElev.setElevData(newData);

        assertEquals("Hege Støfring, 3A", testElev.toString());

        newData = testElev.getElevData();
        assertEquals(newData[0], "Hege");
        assertEquals(newData[1], "Støfring");
        assertEquals(newData[2],"12.04.1684");
        assertEquals(newData[3],"92345612");
        assertEquals(newData[4],"");
    }

    @Test
    void testGetElevNavn(){
        Elev testElev = new Elev("Lars", "Sverkeli", "16.05.1989", "92345612", "", new Klasse(new Trinn("3"), "a"));
        assertEquals("Lars Sverkeli", testElev.getElevNavn());
    }

    @Test
    void testAddRemoveEditForesatt(){
        LocalDate date = LocalDate.now();
        Elev testElev = new Elev("Lars", "Sverkeli", "16.05.1989", "92345612", "", new Klasse(new Trinn("3"), "a"));
        String[] foresattInfo1 = {"Test", "2", "94639843", "post@post", "Gokkistan"};
        testElev.addForesatt(foresattInfo1);
        assertEquals(1, testElev.getForesatte().size());

        String[] foresattInfo2 = {"Tass", "2", "94639865", "post@t", "Gokki"};
        testElev.addForesatt(foresattInfo2);

        assertEquals(2, testElev.getForesatte().size());

        String[] foresatt1 = testElev.getForesattData(testElev.getForesatte().get(0));

        for (int i = 0; i < foresatt1.length; i++) {
            assertEquals(foresatt1[i], foresattInfo1[i]);
        }

        testElev.removeForesatt(0);

        assertEquals(1, testElev.getForesatte().size());

        foresatt1 = testElev.getForesattData(testElev.getForesatte().get(0));

        for (int i = 0; i < foresatt1.length; i++) {
            assertEquals(foresatt1[i], foresattInfo2[i]);
        }

        testElev.setForesattData(testElev.getForesatte().get(0), foresattInfo1);
        foresatt1 = testElev.getForesattData(testElev.getForesatte().get(0));

        for (int i = 0; i < foresatt1.length; i++) {
            assertEquals(foresatt1[i], foresattInfo1[i]);
        }
    }

}
