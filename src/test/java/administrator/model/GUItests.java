package administrator.model;

import org.junit.jupiter.api.Test;

public class GUItests {
    /*
     * As GUI is notoriously hard to test using JUnit tests, the following document will describe a few procedures to test
     * that the progrem functions as intended.
     *
     * While this is mainly a test of the graphical components, proper functionality here depend on the underlying model working as intended
     * If these tests perform as expected, it can also be seen as a confirmation that the model is indeed working.
     *
     * If you want to start testing with e "clean" model, delete "data.ser" before starting.
     * Subsequent steps here will assume you started with a clean run, and did not delete data.ser at any point after
     *
     * You can of course input whatever you want in the boxed, I will generally use single letters for speed
     */

    @Test
    void testAdminFrame(){
        /*
        When running main, you should get a stack trace with a FileNotFoundException. This is expected, as you deleted the save file.

        Try first making an elev, and then a klasse. You should get error popups in both cases.
        Make a Trinn, call it "1"
        Try making Trinn 1 again, you should not be able to

        Make klasse "a" in 1. trinn

        Make an elev in 1a. Input {a, b, c, d, e}.
        This should give an error for bithday. Leave it blank and press ok.
        You should be prompted to make a "foresatt" (Registrer foresatt for a b, 1A".

        Enter {a, b, c, d, e}. You should be prompted to add another. Press ja and enter only first name. Decline the prompt.

        Press "generer epostliste", you should see a text box with "d" (from a b's first parent)

        Add no note and open it, check that the correct text is shown with the correct time stamp.

        Add a new trinn, add a new klasse to that trinn and a new elev to that klasse

        Now, check in "velg elev", "velg klasse" and "velg trinn" that you have two options each place.

        Remove the newly added elev and see that it can no longer be opened. Make a new elev.

        Remove the newly added klasse, and see that you now only have 1 klasse and 1 elev available. Make them again.

        Remove the Trinn, and see that you now only have one trinn, one klasse and one elev.

        Close the program and restart. You should still have the same available.
        You will again get an exception thrown, but from this point on you should get no more exceptions

        To your Trinn, add a new Klasse

        Add Elev so you have at least 5 elev in each klasse. Add epost to at least some of their parents
        Note: you only need fornavn to make elev, no need to input all the data. You also don't have to add Foresatt

         */
    }

    @Test
    void testTrinnFrame(){
        /*
        Open the only Trinn you have

        Click "generer epostliste" and confirm that your entered eposts show up

        click "Lag Grupper"
        Try with groups of 2, 4 and 5. In each case, you should get a mix of Elev from the different classes
        When making groups for 4, 2 Elev will be in the final group.
        Check there is no duplicates

        Make a new klasse. See that you can't duplicate one, and that you successfully can make a new one.

        Make a note and see that you can open it with the correct time stamp
         */
    }

    @Test
    void testKlasseFrame(){
        /*
        Open any klasse with Elev added, see that they are present in the field.
        Click on an Elev in the list, and see that the key information added when the Elev was added is there

        Add notat, both for klasse and elev. See that both open, without conflict, and are separate.
        Click "generer epost" and see that epost for the Foresatte that was created with epost is present

        Add more elev until you have 8-10 in the class

        Click "registrer fravær" and click ok, leave all boxes blank. Click "see fraværsliste" and check that every entry is green.
        "registrer fravær" again, this time add some "s" and some "x".
        See that you get a mix of yellow and red, and that earlier entries are moved one to the right

        **************************************************************************************************************************
        *********** CURRENTLY, YOU CAN ONLY EDIT THE MOST RECENTLY ADDED ENTRY IF SEVERAL ENTRIES HAVE THE SAME NAME *************
        click "rediger", see that current entry is "ok" for all. Change some of them and check that it appears in the overview. They should no longer be green.
        **************************************************************************************************************************

        Click Kopier and check that the overview is correctly pasted into a spreadsheet.

        Do the same for Lekse. Click "opprett lekse" 3 times and add different input.
        Check that in "se lekseoversikt", you have three entries but no data for any Elev

        Click "rediger lekse" and choose one of them to edit. Enter som data, add in some "OK" (green), "x" (red) and "s" (yellow9:
        See that the correct input shows in lekseoversikt and that it can be copied to spreadsheet

        Select an Elev, add three of the other elev as inkampatibel for this elev.
        See that all inkompatible shows up in the elev key info field, and that this elev show up for the three others.

        Test the group maker with various group sizes, make sure no incompatible elev are grouped
         */
    }

    @Test
    void testElevFrame(){
        /*
        Open an Elev you have data stored on. See that the data is there.
        Open notes, see that they are the same notes.

        Check that you can edit personal info, add and edit Foresatt.

        Check that you can add and remove inkompatible.

        All of this should immediately update on the text field.
         */
    }
}
