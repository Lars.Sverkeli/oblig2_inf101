package administrator.GUI;

import java.awt.*;

/**
 * This is a class used to define colors associated with strings. Each Object of this class has an ArrayList of strings, and an associated color.
 * Used to paint JLabels if the value of the label corresponds to a String
 */
public class ColorScheme {
    public final Color color;
    public final String[] strings;

    /**
     * Constructor for the ColorScheme class
     * @param strings Array of strings to be assigned to this color
     * @param color this color
     */
    public ColorScheme(String[] strings, Color color){
        this.strings = strings;
        this.color = color;
    }
}
