package administrator.GUI;

import administrator.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

public class ElevGUI  extends JPanel implements ActionListener {

    private IElevAdministrator elev;
    private final JButton addForesatt = new JButton();
    private final JButton redigerForesatt = new JButton();
    private final JButton redigerElev = new JButton();
    private final JButton addNotat = new JButton();
    private final JButton getNotater = new JButton();
    private final JButton removeForesatt = new JButton();
    private final JButton addNegativ = new JButton();
    private final JButton removeNegativ = new JButton();
    private final JTextPane elevData = new JTextPane();
    private final JLabel elevLabel = new JLabel();

    public ElevGUI(IElevAdministrator elev) {
        this.elev = elev;
        this.initialize();
    }

    private void initialize() {
        JFrame elevFrame = new JFrame(elev.toString());
        initializeButtons(elevFrame);
        initializeTextArea(elevFrame);
        initializeJLabel(elevFrame);
        elevFrame.setLayout(null);
        elevFrame.setSize(800,950);
        elevFrame.setVisible(true);
    }

    private void initializeJLabel(JFrame frame) {
        Font font = new Font("SansSerif", Font.BOLD, 30);
        elevLabel.setBounds(100,10,400,100);
        elevLabel.setFont(font);
        elevLabel.setText(elev.toString());
        frame.add(elevLabel);
    }

    private void initializeTextArea(JFrame elevFrame) {
        String elevText = displayText(elev);
        elevData.setBounds(350, 100, 350,750);
        elevData.setEditable(false);
        elevData.setText(elevText);
        elevFrame.add(elevData);
    }

    private void initializeButtons(JFrame elevFrame) {
        redigerElev.setBounds(100, 100, 200, 50);
        GUIHelpers.addButtonToFrame(redigerElev, "Rediger elevinformasjon", elevFrame, this);
        addForesatt.setBounds(100, 230, 200, 50);
        GUIHelpers.addButtonToFrame(addForesatt, "Legg til foresatt", elevFrame, this);
        redigerForesatt.setBounds(100, 290, 200, 50);
        GUIHelpers.addButtonToFrame(redigerForesatt, "Rediger foresatt", elevFrame, this);
        removeForesatt.setBounds(100, 350, 200, 50);
        GUIHelpers.addButtonToFrame(removeForesatt, "Fjern foresatt", elevFrame, this);
        addNotat.setBounds(100, 480, 200, 50);
        GUIHelpers.addButtonToFrame(addNotat, "Legg til elevnotat", elevFrame, this);
        getNotater.setBounds(100, 540, 200, 50);
        GUIHelpers.addButtonToFrame(getNotater, "Se elevnotater", elevFrame, this);
        addNegativ.setBounds(100, 670, 200, 50);
        GUIHelpers.addButtonToFrame(addNegativ, "Legg til inkompatibel elev", elevFrame, this);
        removeNegativ.setBounds(100, 730, 200, 50);
        GUIHelpers.addButtonToFrame(removeNegativ, "Fjern inkompatibel elev", elevFrame, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addForesatt) {
            addNewForesatt(elev);
        } else if (e.getSource() == redigerForesatt) {
            redigerForesatt();
        } else if (e.getSource() == removeForesatt) {
            removeForesatt();
        } else if (e.getSource() == redigerElev) {
            redigerElev();
        } else if (e.getSource() == addNotat) {
            addNotat();
        } else if (e.getSource() == getNotater) {
            getNotater();
        } else if (e.getSource() == addNegativ) {
            addNegativ();
        } else if (e.getSource() == removeNegativ) {
            removeNegativ();
        }
        elevData.setText(displayText(elev));
    }

    private void addNotat(){
        String notat = GUIHelpers.getInputFromTextArea("Skriv inn notat");
        if (notat != null) {
            elev.addNotat(notat);
        }
    }

    private void getNotater(){
        String notater = Helpers.arrayListAsSingleString(elev.getNotater(), "\n");
        GUIHelpers.showTextField(notater, "notater for " + this.elev.toString());

    }

    private void addNegativ() {
        int elev2 = GUIHelpers.getDropmenu(elev.getTrinnElever(), "Hvilken elev skal legges til som inkompatibel med " + elev.getElevNavn(), "Velg elev", "Finner ingen annen elev");
        if (elev2 == -1){
            return;
        }
        elev.addNegativ(elev2);
    }

    private void removeNegativ(){
        String[] negative = elev.getNegative();
        if (negative.length == 0) {
            GUIHelpers.simpleDialogBox(elev.toString() + " har ingen inkompatible elever lagt til");
            return;
        }

        int index = GUIHelpers.getDropmenu(negative, "Hvilken elev skal fjernes som inkompatibel med " + elev.toString() + "?", "Velg elev", "Finner ingen annen elev");
        if (index == -1){
            return;
        }
        elev.negativToRemove(index);

    }

    private void removeForesatt(){
        String[] foresatte = Helpers.asStringArray(elev.getForesatte());
        if (foresatte.length == 0) {
            GUIHelpers.simpleDialogBox(elev.toString() + " har ingen foresatte lagt til");
            return;
        }

        int index = GUIHelpers.getDropmenu(foresatte, "Hvilken foresatt skal fjernes?", "Velg foersatt", "");
        if (index != -1){
            int result = GUIHelpers.simpleConfirmBox("Vil du virkelig slette " + foresatte[index] + "?","Slett foresatt?");
            if (result == JOptionPane.YES_OPTION) {
                elev.removeForesatt(index);
            }
        }
    }

    private void redigerForesatt() {
        ArrayList<Foresatt> foresatte = elev.getForesatte();
        String[] foresatteAsString = Helpers.asStringArray(foresatte);
        int index = GUIHelpers.getDropmenu(foresatteAsString, "Hvilken foresatt vil du redigere?", "Velg foresatt", "Ingen foresatt");
        if (index == -1) {
            return;
        }
        Foresatt foresatt = foresatte.get(index);

        String[] foreldreData = elev.getForesattData(foresatt);
        String[] foreldreFelter = {"Fornavn: ", "Etternavn: ", "Telefon:", "Epost:", "Adresse: " };
        ArrayList<String> output = GUIHelpers.getInputFromList(foreldreFelter,"Registrer foresatt for " + elev.toString(), foreldreData);
        if(output == null){
            return;
        }
        if(output.get(0).equals("") || output.get(1).equals("")) {
            GUIHelpers.simpleDialogBox("Både fornavn og etternavn må fylles ut");
            addNewForesatt(elev);
            return;
        } else {
            elev.setForesattData(foresatt, output.toArray(new String[0]));
        }
    }

    private void redigerElev() {
        String[] elevData = elev.getElevData();
        String[] elevFelter = {"Fornavn: ", "Etternavn: ", "Fødselsdag:", "Telefon:", "Kommentar: " };
        ArrayList<String> output = GUIHelpers.getInputFromList(elevFelter,"Registrer " + elev.toString(), elevData);
        if(output == null){
            return;
        }
        if(output.get(0).equals("")) {
            GUIHelpers.simpleDialogBox("Fornavn må fylles ut");
            redigerElev();
            return;
        } else if ((Helpers.parseDate(output.get(2)) == null) && !output.get(2).equals("")) {
        GUIHelpers.simpleDialogBox("Fødselsdag må skrives på formatet dd.mm.yyyy. F.eks 16.05.1989");
        redigerElev();
        return;
        } else {
            elev.setElevData(output.toArray(new String[0]));
        }
    }

    public static String displayText(IElevAdministrator elev){
        return elev.getElevKort();
    }

    public static void addNewForesatt(IElevAdministrator elev) {
        String[] foreldreFelter = {"Fornavn: ", "Etternavn: ", "Telefon:", "Epost:", "Adresse: " };
        ArrayList<String> output = GUIHelpers.getInputFromList(foreldreFelter,"Registrer foresatt for " + elev.toString());
        if(output == null){
            return;
        } else if (Collections.frequency(output, ("")) == output.size()){
            return;
        } else {
            elev.addForesatt(output.toArray(new String[0]));
        }
    }
}
