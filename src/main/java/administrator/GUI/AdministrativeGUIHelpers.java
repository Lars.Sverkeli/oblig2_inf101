package administrator.GUI;

import administrator.model.*;

import javax.swing.*;
import java.util.ArrayList;

public class AdministrativeGUIHelpers {

    /**
     * Helper method
     * Prints a dialog box for input of group size and calls the group maker with the input and provided list of Elev
     * Then shows the output of the group maker in a fext field on screen
     * @param elever List of elever to be sorted into groups
     */
    public static void lagGrupper(ArrayList<IElevReader> elever) {
        int gruppeStorrelse = GUIHelpers.getIntegerFromDialogBox("Hvor store grupper vil du lage?", elever.size() / 2);
        if (gruppeStorrelse == 0) {
            return;
        }
        ArrayList<ArrayList<IElevReader>> grupper = Helpers.makeElevGroups(elever,gruppeStorrelse);
        String grupperAsString = Helpers.nestedArrayListAsString(grupper, "\n", "\n");
        if( grupperAsString == null) {
            GUIHelpers.showTextField("Kunne ikke lage grupper", "Grupper");
        } else {
            GUIHelpers.showTextField(grupperAsString, "Grupper");
        }
    }

    /**
     * Methos do add new elev to a Klasse, here represented by the interface IKlasseAdministrator.
     * This method generates an array of strings needed to call the addElev() method from a dialog box and calls for the new Elev to be added
     * Added as a separate method here as several UI elements makes use of this functionality
     * @param klasse recieving a new elev
     */
    public static void addNewElev(IKlasseAdministrator klasse) {
        ArrayList<String> output = GUIHelpers.getElevInput();
        if(output == null) {
            return;
        } else {
            klasse.addElev(output.toArray(new String[0]));
            ElevGUI.addNewForesatt(klasse.getLastElev());
            while (true) {
                JFrame confirmBox = new JFrame();
                int result = JOptionPane.showConfirmDialog(confirmBox, "Vil du legge til flere foresatte for " + klasse.getLastElev().toString() + "?", "Legg til flere foresatte?", JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    ElevGUI.addNewForesatt(klasse.getLastElev());
                } else {
                    break;
                }
            }
        }
        klasse.sortElever();
    }

    /**
     * Gets a dialog box for the input required to make a new Klasse and calls the proper methods for adding a new Klasse to a trinn.
     * Used by more than one UI element, placed here to avoid repetition.
     * @param trinn
     */
    public static void addNewKlasse(ITrinnAdministrator trinn) {
        JFrame inputBox = new JFrame();
        String id = JOptionPane.showInputDialog(inputBox, "Hvilken klasse skal legges til?");
        if (id == null){
            return;
        }
        if (trinn.checkIfKlasseExist(id)) {
            GUIHelpers.simpleDialogBox("Klassen finnes allerede");
            return;
        }
        trinn.addKlasse(id);
        trinn.sortKlasser();
    }

}
