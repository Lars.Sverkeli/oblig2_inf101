
package administrator.GUI;

import administrator.model.Helpers;
import administrator.model.ITrinnAdministrator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TrinnGUI extends JPanel implements ActionListener {

    private ITrinnAdministrator trinn;
    private final JButton newKlasse = new JButton();
    private final JButton openKlasse = new JButton();
    private final JButton addNotat = new JButton();
    private final JButton getNotater = new JButton();
    private final JButton lagGrupper = new JButton();
    private final JButton lagEpostliste = new JButton();
    private final JLabel trinnLabel = new JLabel();


    TrinnGUI(ITrinnAdministrator trinn) {
        this.trinn = trinn;
        this.initialize();
    }


    private void initialize() {
        JFrame trinnFrame = new JFrame(trinn.toString());
        initializeButtons(trinnFrame);
        initializeJLabels(trinnFrame);
        trinnFrame.setLayout(null);
        trinnFrame.setSize(400,750);
        trinnFrame.setVisible(true);
    }

    private void initializeJLabels(JFrame frame) {
        Font font = new Font("SansSerif", Font.BOLD, 30);
        trinnLabel.setBounds(100,10,400,100);
        trinnLabel.setFont(font);
        trinnLabel.setText(trinn.toString());
        frame.add(trinnLabel);
    }

    private void initializeButtons(JFrame trinnFrame) {
        newKlasse.setBounds(100, 100, 200, 50);
        GUIHelpers.addButtonToFrame(newKlasse, "Legg til klasse", trinnFrame, this);
        openKlasse.setBounds(100, 170, 200, 50);
        GUIHelpers.addButtonToFrame(openKlasse, "Åpne klasse", trinnFrame,this);
        addNotat.setBounds(100, 240, 200, 50);
        GUIHelpers.addButtonToFrame(addNotat,"Skriv notat", trinnFrame,this);
        getNotater.setBounds(100, 310, 200, 50);
        GUIHelpers.addButtonToFrame(getNotater,"Se notater", trinnFrame,this);
        lagGrupper.setBounds(100, 380, 200, 50);
        GUIHelpers.addButtonToFrame(lagGrupper,"Lag grupper", trinnFrame,this);
        lagEpostliste.setBounds(100, 450, 200, 50);
        GUIHelpers.addButtonToFrame(lagEpostliste,"Generer epostliste", trinnFrame,this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == openKlasse) {
            openExistingKlasse();
        } else if (e.getSource() == newKlasse) {
            addNewKlasse();
        } else if (e.getSource() == addNotat) {
            addNotat();
        } else if (e.getSource() == getNotater) {
            getNotater();
        } else if (e.getSource() == lagGrupper) {
            lagGrupper();
        } else if (e.getSource() == lagEpostliste) {
            lagEpostliste();
        }
    }

    private void openExistingKlasse() {
        String[] klasser = Helpers.asStringArray(trinn.getRestrictedKlasser());
        int index = GUIHelpers.getDropmenu(klasser, "Hvilken klasse vil du åpne?", "velg Klasse", "Finner ingen klasse");
        if (index != -1) {
            KlasseGUI klasseFrame = new KlasseGUI(trinn.getKlasseAdministrator(index));
        }
    }

    private void addNewKlasse() {
        AdministrativeGUIHelpers.addNewKlasse(this.trinn);
    }

    private void addNotat() {
        String notat = GUIHelpers.getInputFromTextArea("Skriv inn notat");
        if (notat != null) {
            trinn.addNotat(notat);
        }
    }

    private void getNotater() {
        String notater = Helpers.arrayListAsSingleString(trinn.getNotater(), "\n");
        GUIHelpers.showTextField(notater, "notater");
    }

    private void lagGrupper() {
        AdministrativeGUIHelpers.lagGrupper(trinn.getReadOnlyElever());
    }

    private void lagEpostliste() {
        GUIHelpers.showTextField(trinn.getEpost(), "epostliste");
    }
}