package administrator.GUI;

import administrator.model.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class KlasseGUI extends JPanel implements ActionListener, ListSelectionListener {

    private IKlasseAdministrator klasse;
    private final JButton newElev = new JButton();
    private final JButton openElev = new JButton();
    private final JButton addNotat = new JButton();
    private final JButton getNotater = new JButton();
    private final JButton addElevNotat = new JButton();
    private final JButton getElevNotater = new JButton();
    private final JButton getTilstede = new JButton();
    private final JButton registrerTilstede = new JButton();
    private final JButton opprettLekse = new JButton();
    private final JButton registrerLekse = new JButton();
    private final JButton seLekseOversikt = new JButton();
    private final JButton lagGrupper = new JButton();
    private final JButton setNegativ = new JButton();
    private final JButton oppdaterTilstede = new JButton();
    private final JButton lekserToClipboard = new JButton();
    private final JButton tilstedeToClipboard = new JButton();
    private final JButton genererEpost = new JButton();
    private final JList elevListe = new JList();
    private final JTextPane textField = new JTextPane();
    private final JLabel klasseLabel = new JLabel();
    private final JLabel elevLabel = new JLabel();
    private final JLabel tilstedeLabel = new JLabel();
    private final JLabel lekseLabel = new JLabel();
    private final JLabel notatLabel = new JLabel();

    KlasseGUI(IKlasseAdministrator klasse) {
        this.klasse = klasse;
        this.initialize();
    }

    private void initialize() {
        JFrame klasseFrame = new JFrame(klasse.toString());
        initializeButtons(klasseFrame);
        initializeJlist(klasseFrame);
        initializeTextField(klasseFrame);
        initializeJLabels(klasseFrame);
        klasseFrame.setLayout(null);
        klasseFrame.setSize(1200,750);
        klasseFrame.setVisible(true);
    }

    private void initializeJLabels(JFrame frame) {
        Font font = new Font("SansSerif", Font.BOLD, 30);
        klasseLabel.setBounds(100,10,400,100);
        GUIHelpers.addLabelToFrame(klasseLabel, frame, klasse.toString(), font, JLabel.LEFT);

        elevLabel.setBounds(700,10,400,100);
        GUIHelpers.addLabelToFrame(elevLabel, frame, "", font, JLabel.RIGHT);

        font = new Font("SansSerif", Font.BOLD, 20);
        tilstedeLabel.setBounds(100,100,200,50);
        tilstedeLabel.setFont(font);
        tilstedeLabel.setText("Fraværsregistrering");
        tilstedeLabel.setHorizontalAlignment(JLabel.CENTER);
        tilstedeLabel.setVerticalAlignment(JLabel.TOP);
        frame.add(tilstedeLabel);

        lekseLabel.setBounds(100,270,200,50);
        lekseLabel.setFont(font);
        lekseLabel.setText("Lekseregistrering");
        lekseLabel.setHorizontalAlignment(JLabel.CENTER);
        lekseLabel.setVerticalAlignment(JLabel.TOP);
        frame.add(lekseLabel);

        notatLabel.setBounds(100,440,200,50);
        notatLabel.setFont(font);
        notatLabel.setText("Notater");
        notatLabel.setHorizontalAlignment(JLabel.CENTER);
        notatLabel.setVerticalAlignment(JLabel.TOP);
        frame.add(notatLabel);
    }

    private void initializeTextField(JFrame frame) {
        textField.setBounds(600,100,250,550);
        textField.setEditable(false);
        frame.add(textField);
    }

    private void initializeJlist(JFrame frame) {
        setElevListeData();
        elevListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        elevListe.addListSelectionListener(this);
        elevListe.setBounds(350,100,200,550);
        frame.add(elevListe);
    }

    private void initializeButtons(JFrame klasseFrame) {
        registrerTilstede.setBounds(100, 130, 150, 50);
        GUIHelpers.addButtonToFrame(registrerTilstede, "Registrer fravær", klasseFrame,this);

        oppdaterTilstede.setBounds(250, 130, 50, 50);
        GUIHelpers.addButtonToFrame(oppdaterTilstede, "Rediger", klasseFrame, this);

        getTilstede.setBounds(100, 190, 150, 50);
        GUIHelpers.addButtonToFrame(getTilstede, "Se fraværsliste", klasseFrame, this);

        tilstedeToClipboard.setBounds(250, 190, 50, 50);
        GUIHelpers.addButtonToFrame(tilstedeToClipboard, "Kopier", klasseFrame, this);

        opprettLekse.setBounds(100, 300, 100, 50);
        GUIHelpers.addButtonToFrame(opprettLekse, "Opprett lekse", klasseFrame, this);

        registrerLekse.setBounds(200, 300, 100, 50);
        GUIHelpers.addButtonToFrame(registrerLekse, "Registrer lekse", klasseFrame, this);

        seLekseOversikt.setBounds(100, 360, 150, 50);
        GUIHelpers.addButtonToFrame(seLekseOversikt, "Se Lekseoversikt", klasseFrame, this);

        lekserToClipboard.setBounds(250, 360, 50, 50);
        GUIHelpers.addButtonToFrame(lekserToClipboard, "kopier", klasseFrame, this);

        addNotat.setBounds(100, 470, 150, 50);
        GUIHelpers.addButtonToFrame(addNotat, "Legg til klassenotat", klasseFrame, this);

        getNotater.setBounds(250, 470, 50, 50);
        GUIHelpers.addButtonToFrame(getNotater, "Åpne", klasseFrame, this);

        genererEpost.setBounds(100, 530, 200, 50);
        GUIHelpers.addButtonToFrame(genererEpost, "Generer epostliste", klasseFrame, this);

        openElev.setBounds(900, 100, 200, 50);
        GUIHelpers.addButtonToFrame(openElev, "Åpne elev", klasseFrame,this);

        addElevNotat.setBounds(900, 170, 200, 50);
        GUIHelpers.addButtonToFrame(addElevNotat, "Legg til elevnotat", klasseFrame, this);

        getElevNotater.setBounds(900, 240, 200, 50);
        GUIHelpers.addButtonToFrame(getElevNotater, "Se notat for denne eleven", klasseFrame, this);

        setNegativ.setBounds(900, 380, 200, 50);
        GUIHelpers.addButtonToFrame(setNegativ, "Legg til inkompatibel elev", klasseFrame, this);

        lagGrupper.setBounds(900, 450, 200, 50);
        GUIHelpers.addButtonToFrame(lagGrupper, "Lag grupper", klasseFrame, this);

        newElev.setBounds(900, 590, 200, 50);
        GUIHelpers.addButtonToFrame(newElev, "Legg til elev", klasseFrame, this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == registrerTilstede){
            registrerTilstede();
        } else if (e.getSource() == oppdaterTilstede){
            oppdaterTilstede();
        } else if (e.getSource() == getTilstede){
            showTilstedeListe();
        } else if (e.getSource() == tilstedeToClipboard){
            tilstedeToClipboard();
        } else if (e.getSource() == opprettLekse){
            opprettLekse();
        } else if (e.getSource() == registrerLekse){
            registrerLekse();
        } else if (e.getSource() == seLekseOversikt){
            seLekseOversikt();
        } else if (e.getSource() == lekserToClipboard){
            lekserToClipboard();
        } else if (e.getSource() == addNotat){
            addNewNotat();
        } else if (e.getSource() == getNotater){
            showNotat();
        } else if (e.getSource() == getElevNotater){
            showElevNotater();
        } else if (e.getSource() == genererEpost){
            genererEpost();
        } else if (e.getSource() == openElev) {
            openExistingElev();
        } else if (e.getSource() == addElevNotat){
            addElevNotat();
        } else if (e.getSource() == setNegativ){
            setNegativ();
        } else if (e.getSource() == lagGrupper){
            lagGrupper();
        } else if (e.getSource() == newElev) {
            addNewElev();
            setElevListeData();
        }
        IElevReader elev = getElevFromElevListe();
        if (elev != null){
            textField.setText(elev.getElevKort());
            elevLabel.setText(getElevFromElevListe().toString());
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        IElevReader elev = getElevFromElevListe();
        if (elev != null){
            textField.setText(elev.getElevKort());
            elevLabel.setText(getElevFromElevListe().toString());
        }
    }

    private IElevReader getElevFromElevListe(){
        int index = elevListe.getSelectedIndex();
        if(index > -1){
            IElevReader elev = klasse.getReadOnlyElever().get(index);
            return elev;
        } else {
            return null;
        }
    }

    private void setElevListeData() {
        ArrayList<String> elever = klasse.getElevNavn();
        elevListe.setListData(Helpers.asStringArray(elever));
    }



    private void registrerTilstede(){
        ArrayList<String> elever = klasse.getElevNavn();
        if (isEleverEmpty()) {
            return;
        }
        String dato = Helpers.getDateAsString();

        JFrame inputBox = new JFrame();
        dato = JOptionPane.showInputDialog(inputBox, "Hvilken dato?", dato);
        if (dato == null){
            return;
        }
        ArrayList<String> output =  GUIHelpers.getInputFromList(Helpers.asStringArray(elever),"Registrer fravær for " + dato);
        if (output == null) {
            return;
        }
        klasse.registrerTilstede(dato, output);
    }


    private void oppdaterTilstede() {
        ArrayList<String> elever = klasse.getElevNavn();
        if (isEleverEmpty()) {
            return;
        }
        int index = GUIHelpers.getDropmenu(Helpers.asStringArray(klasse.getTilstede()), "Hvilken dato vil du åpne?", "Velg dato", "Klassen har ingen registrerte lekser");
        if (index == -1) {
            return;
        }
        ArrayList<String> currentData = klasse.getSingleTilstede(index);
        ArrayList<String> output = GUIHelpers.getInputFromList(Helpers.asStringArray(elever),"Registrer fravær", Helpers.asStringArray(currentData));
        if (output == null) {
            return;
        }
        klasse.setSingleTilstede(index, output);
    }


    private void showTilstedeListe() {
        ArrayList<ColorScheme> colorSchemes = getTilstedeColorScheme();
        if (isEleverEmpty()) {
            return;
        }
        ArrayList<String> elever = klasse.getElevNavn();
        GUIHelpers.showPanelForNestedArray(klasse.getTilstede(), elever, klasse.getTilstedeOversikt(), "Fraværslogg for " + klasse.toString(), colorSchemes);
    }


    private void tilstedeToClipboard() {
        String tilstedeOversikt = "Fraværsoversikt for " + klasse.toString() + "\t";
        tilstedeOversikt = tilstedeOversikt + Helpers.arrayListAsSingleString(klasse.getTilstede(), "\t") + "\n";

        ArrayList<String> navn = klasse.getElevNavn();
        ArrayList<ArrayList<String>> data = klasse.getTilstedeOversikt();
        for (int i = 0; i < navn.size(); i++) {
            tilstedeOversikt = tilstedeOversikt + navn.get(i) + "\t" +
                    Helpers.arrayListAsSingleString(data.get(i), "\t") + "\n";
        }
        GUIHelpers.addToClipboard(tilstedeOversikt);
    }


    private void opprettLekse() {
        String[] prompt = new String[]{"Fag:", "Uke:"};
        ArrayList<String> output = GUIHelpers.getInputFromList(prompt, "Skriv inn lekseinformasjon");

        if (output == null) {
            return;
        }

        String lekse = output.get(0) + ", Uke " + output.get(1);

        klasse.nyLekse(lekse);
    }


    private void registrerLekse() {
        int index = GUIHelpers.getDropmenu(Helpers.asStringArray(klasse.getLekser()), "Hvilken lekse vil du åpne?", "Velg lekse", "Ingen lekse er registrert");
        if (index == -1) {
            return;
        }
        String[] currentData = Helpers.asStringArray(klasse.getSingleLekse(index));
        ArrayList<String> output = GUIHelpers.getInputFromList(Helpers.asStringArray(klasse.getElevNavn()),"Registrer lekse", currentData);
        if (output == null) {
            return;
        }
        klasse.registrerLekse(output, index);
    }


    private void seLekseOversikt() {
        ArrayList<ColorScheme> colorSchemes = getLekseColorScheme();
        if (isEleverEmpty()) {
            return;
        }
        ArrayList<String> elevNavn = klasse.getElevNavn();
        GUIHelpers.showPanelForNestedArray(klasse.getLekser(), elevNavn, klasse.getLekseListe(), "Lekseoversikt for " + klasse.toString(), colorSchemes);
    }


    private void lekserToClipboard() {
        String lekseOversikt = "Lekseoversikt for " + klasse.toString() + "\t";
        lekseOversikt = lekseOversikt + Helpers.arrayListAsSingleString(klasse.getLekser(), "\t") + "\n";

        ArrayList<String> navn = klasse.getElevNavn();
        ArrayList<ArrayList<String>> data = klasse.getLekseListe();
        for (int i = 0; i < navn.size(); i++) {
            lekseOversikt = lekseOversikt + navn.get(i) + "\t" +
                    Helpers.arrayListAsSingleString(data.get(i), "\t") + "\n";
        }
        GUIHelpers.addToClipboard(lekseOversikt);
    }


    private void addNewNotat() {
        String notat = GUIHelpers.getInputFromTextArea("Skriv inn notat");
        if (notat != null) {
            klasse.addNotat(notat);
        }
    }


    private void showNotat() {
        String notater = Helpers.arrayListAsSingleString(klasse.getNotater(), "\n");
        GUIHelpers.showTextField(notater, "notater for " + this.klasse.toString());
    }


    private void genererEpost() {
        GUIHelpers.showTextField(klasse.getEpost(), "epostliste");
    }


    private void openExistingElev() {
        int index = elevListe.getSelectedIndex();
        if(index == -1){
            GUIHelpers.simpleDialogBox("Velg en elev");
            return;
        }
        ElevGUI elevFrame = new ElevGUI(klasse.getIElev(index));
    }


    private void addElevNotat() {
        int elevIndex = elevListe.getSelectedIndex();
        String notat = GUIHelpers.getInputFromTextArea("Skriv inn notat");
        if (notat != null) {
            klasse.addElevNotat(elevIndex, notat);
        }
    }


    private void showElevNotater() {
        int elevIndex = elevListe.getSelectedIndex();
        String notater = Helpers.arrayListAsSingleString(klasse.getElevNotat(elevIndex), "\n");
        GUIHelpers.showTextField(notater, "notater for " + klasse.getReadOnlyElever().get(elevIndex).toString());
    }


    private void setNegativ() {
        int elev1 = elevListe.getSelectedIndex();
        if (elev1 == -1){
            return;
        }

        int elev2 = GUIHelpers.getDropmenu(Helpers.asStringArray(klasse.getReadOnlyElever()), "Hvilken elev skal legges til som inkompatibel med " + klasse.getElevNavn().get(elev1), "Velg elev", "Finner ingen annen elev");
        if (elev2 == -1){
            return;
        }

        klasse.addNegativ(elev1, elev2);
    }

    private void lagGrupper() {
        AdministrativeGUIHelpers.lagGrupper(klasse.getReadOnlyElever());
    }


    private void addNewElev() {
        AdministrativeGUIHelpers.addNewElev(this.klasse);
    }


    private ArrayList<ColorScheme> getLekseColorScheme() {
        ArrayList<ColorScheme> colorSchemes = new ArrayList<>();
        String[] greens = {"ok", "OK", "Ok"};
        colorSchemes.add(new ColorScheme(greens, Color.GREEN));
        String[] yellows = {"S", "s"};
        colorSchemes.add(new ColorScheme(yellows, Color.YELLOW));
        String[] reds = {"X","x"};
        colorSchemes.add(new ColorScheme(reds, Color.RED));
        return colorSchemes;
    }


    private ArrayList<ColorScheme> getTilstedeColorScheme() {
        ArrayList<ColorScheme> colorSchemes = new ArrayList<>();
        String[] greens = {"ok", "OK", "Ok"};
        colorSchemes.add(new ColorScheme(greens, Color.GREEN));
        String[] yellows = {"S", "s"};
        colorSchemes.add(new ColorScheme(yellows, Color.YELLOW));
        String[] reds = {"X","x"};
        colorSchemes.add(new ColorScheme(reds, Color.RED));
        return colorSchemes;
    }


    private boolean isEleverEmpty() {
        if (klasse.getElevNavn().size() == 0) {
            GUIHelpers.simpleDialogBox("Klassen har ingen elever");
            return true;
        }
        return false;
    }
}