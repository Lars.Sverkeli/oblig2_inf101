package administrator.GUI;

import administrator.model.Helpers;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * This class contains helper methods for displaying elements such as dialog boxed, dropdown menus and text fields.
 * Most methods take an input from the user and displays the desired component depending on the input.
 * Some of these methods also return output, such as text from dialog boxed, index chosen from dropdown menus etc.
 *
 * @Author Lars J. Sverkeli
 */

public class GUIHelpers {

    /**
     * Adds a given label to a given frame with a given font.
     *
     * Optional horizontal and vertical alignment
     */
    public static void addLabelToFrame(JLabel label, JFrame frame, String text, Font font, int horizontal, int vertical){
        label.setFont(font);
        label.setText(text);
        frame.add(label);
        label.setHorizontalAlignment(horizontal);
        label.setVerticalAlignment(vertical);
    }
    /**
     * Adds a given label to a given frame with a given font.
     *
     * Optional horizontal and vertical alignment
     */
    public static void addLabelToFrame(JLabel label, JFrame frame, String text, Font font, int horizontal){
        label.setFont(font);
        label.setText(text);
        label.setHorizontalAlignment(horizontal);
        frame.add(label);
    }
    /**
     * Adds a given label to a given frame with a given font.
     *
     * Optional horizontal and vertical alignment
     */
    public static void addLabelToFrame(JLabel label, JFrame frame, String text, Font font){
        label.setFont(font);
        label.setText(text);
        frame.add(label);
    }

    /**
     * Adds a provided text to clipboard
     * @param text to be sent to clipboard
     */
    public static void addToClipboard(String text) {
        String string = text;
        StringSelection selection = new StringSelection(string);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }

    /**
     * Shows a dialog box specific to input needed to register an Elev.
     *
     * @return personal data about an Elev
     */
    public static ArrayList<String> getElevInput() {
        String[] prompt = new String[]{"fornavn:", "etternavn", "Fødselsdag (dd.mm.yyyy):", "Telefon", "Kommentar"};
        ArrayList<String> output = GUIHelpers.getInputFromList(prompt, "Skriv inn personalia");

        if (output == null) {
            return null;
        }

        if (output.get(0).equals("")) {
            GUIHelpers.simpleDialogBox("Fornavn må fylles ut");
            return getElevInput();
        } else if ((Helpers.parseDate(output.get(2)) == null) && !output.get(2).equals("")) {
            GUIHelpers.simpleDialogBox("Fødselsdag må skrives på formatet dd.mm.yyyy. F.eks 16.05.1989");
            return getElevInput();
        } else {
            return output;
        }
    }

    /**
     * Shows a dialog box as a text field and parses the input as an Integer
     *
     * @param title title of the dialog box
     * @param maxNr the highest number allowed as input
     * @return text input parsed as Integer
     */
    public static Integer getIntegerFromDialogBox(String title, int maxNr) {
        String text = JOptionPane.showInputDialog(title);
        if (text == null) {
            return 0;
        }
        try {
            int index = Integer.parseInt(text);
            if (index > maxNr) {
                GUIHelpers.simpleDialogBox("Tallet du anga er for høyt, prøv igjen");
                return getIntegerFromDialogBox(title, maxNr);
            }
            return index;
        } catch (NumberFormatException e) {
            GUIHelpers.simpleDialogBox("Srkiv inn et heltall");
            return getIntegerFromDialogBox(title, maxNr);
        }
    }

    /**
     * Shows a dialog box containing a dropmenu
     *
     * The elements in the dropmenu are given by an array of strings, and this method returns the index of the selected element when yes is pressed.
     *
     * @param elements Elements to be presented in the drop menu
     * @param prompt Text to be shown above the drop menu
     * @param title Title of the dialog box
     * @param dialogIfEmpty Dialog box shown if tere are no elements in the provided array
     * @return index of the selected element
     */
    public static int getDropmenu(String[] elements, String prompt, String title, String dialogIfEmpty) {
        if(elements.length < 1){
            JFrame error = new JFrame();
            JOptionPane.showMessageDialog(error,dialogIfEmpty);
            return -1;
        }

        String elementVelger = (String) JOptionPane.showInputDialog(null,
                prompt,
                title,
                JOptionPane.QUESTION_MESSAGE,
                null,
                elements,
                elements[0]
        );
        if (elementVelger != null) {
            return Helpers.getArrayIndex(elements, elementVelger);
        }
        return  -1;
    }

    /**
     * Method to show a panel representing a grid from an ArrayList of ArrayList<String>.
     *
     * Elements in the outer
     * @param header ArrayList of String denominating the contain in each respective column
     * @param labels ArrayList of String denominating the contain in each respective line
     * @param data Nested array list presented as a grid. Each element in the outer ArrayList is presented on a single line.
     * @param title title of the panel
     */
    public static void showPanelForNestedArray(ArrayList<String> header, ArrayList<String> labels, ArrayList<ArrayList<String>> data, String title) {
        showPanelForNestedArray(header, labels, data, title, null);
    }/**
     * Method to show a panel representing a grid from an ArrayList of ArrayList<String>.
     *
     * Elements in the outer
     * @param header ArrayList of String denominating the contain in each respective column
     * @param labels ArrayList of String denominating the contain in each respective line
     * @param data Nested array list presented as a grid. Each element in the outer ArrayList is presented on a single line.
     * @param title title of the panel
     * @param colorSchemes optional field to provide a color scheme used to color code fields in the shown panel based on content
     */
    public static void showPanelForNestedArray(ArrayList<String> header, ArrayList<String> labels, ArrayList<ArrayList<String>> data,
                                               String title, ArrayList<ColorScheme> colorSchemes) {
        JPanel panel = new JPanel();
        panel.setSize(800,800);
        panel.setLayout(null);
        panel.setVisible(true);
        JLabel navn = new JLabel("Elev");
        navn.setBounds(10,10,80,25);
        panel.add(navn);

        for (int i = 0; i < labels.size(); i++) {
            JLabel label = new JLabel();
            label.setText(labels.get(i));
            label.setBounds(10, 38 + 24 * i, 80, 25);
            panel.add(label);
        }

        for (int i = 0; i < header.size(); i++) {
            JLabel label = new JLabel();
            label.setText(header.get(i));
            label.setBounds(95 + i * 80, 10, 80, 25);
            panel.add(label);
        }

        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < header.size(); j++) {
                if(data.get(i).size() < j + 1) {
                    continue;
                }
                JLabel label = new JLabel();
                label.setText(data.get(i).get(j));
                label.setBounds(95 + j * 80, 38 + 24 * i, 80, 25);
                label.setHorizontalAlignment(JLabel.CENTER);
                if (!(colorSchemes == null)) {
                    Color color = GUIHelpers.getColorFromScheme(colorSchemes ,label.getText());
                    if (color != null){
                        JPanel tile = new JPanel();
                        tile.setBackground(color);
                        tile.setBounds(label.getBounds());
                        panel.add(tile);
                    }
                }
                panel.add(label);
            }
        }

        JFrame frame = new JFrame(title);
        frame.add(panel);
        frame.setSize(800,800);
        frame.setVisible(true);
    }

    /**
     * Determines appropriate color based on in put text and a provided color scheme
     *
     * @param colorSchemes ArrayList of ColorSchemes linking words to a color
     * @param text Text to be assigned color
     * @return Color assigned to the text if text is found in color schemes
     */
    private static Color getColorFromScheme(ArrayList<ColorScheme> colorSchemes, String text) {
        for (ColorScheme colorScheme : colorSchemes) {
            {
                for (String string : colorScheme.strings) {
                    if(string.equals(text)){
                        return colorScheme.color;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Adds a right side scroll par to the provided JComponent
     * @param panel The panel containing the component
     * @param component Component recieving the scroll bar
     */
    public static void addRightSideScrollBar(JPanel panel, JComponent component){
        JScrollPane scroll = new JScrollPane(component);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(scroll);
    }

    /**
     * Shows a text area and returns text entered
     *
     * @param title title of the text box
     * @return String coresponding to text contained in text area when OK is pressed
     */
    public static String getInputFromTextArea(String title){
        JPanel inputBox = new JPanel();
        JTextArea textInput = new JTextArea(20,80);
        inputBox.add(textInput);

        addRightSideScrollBar(inputBox, textInput);

        int result = JOptionPane.showConfirmDialog(null, inputBox, title,JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            return textInput.getText();
        }
        return null;
    }

    /**
     * Shows a non-editable text area containing the provided text
     *
     * @param content String to be shown in the text area
     * @param title Title of the text area
     */
    public static void showTextField(String content, String title){
        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(new EtchedBorder()));
        JTextArea textArea = new JTextArea(20,80);
        textArea.setText(content);
        textArea.setEditable(false);

        addRightSideScrollBar(panel, textArea);
        GUIHelpers.showFrameWithComponent(panel, title);
    }

    /**
     * Shows a simple dialog box with buttons for yes and no
     *
     * @param message Text in the dialog box
     * @param title Title of the dialog box
     * @return Option pressed
     */
    public static int simpleConfirmBox(String message, String title){
        JFrame confirmBox = new JFrame();
        return JOptionPane.showConfirmDialog(confirmBox, message, title, JOptionPane.YES_NO_OPTION);
    }

    /**
     * Shows a simple dialog box showing a given message
     * @param message to be shown in the dialog box
     */
    public static void simpleDialogBox(String message){
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame,message);
    }

    /**
     * Helper to visualize a Jpanel
     * Adds the panel to a JFrame and reveals it.
     * @param panel to be revealed
     * @param title of frame
     */
    public static void showFrameWithComponent(JComponent panel, String title){
        JFrame frame = new JFrame(title);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Adds a JButton without margins to a given JFrame and assigns it to a give ActionListener
     * Used in this model to assigne large numbers of buttons to the same frame and listener.
     *
     * Takes either a String or an Icon as arguments to display
     * @param button
     * @param icon
     * @param frame
     * @param listener
     */
    public static void addButtonToFrame(JButton button, Icon icon, JFrame frame, ActionListener listener) {
        button.setIcon(icon);
        button.setMargin(new Insets(0,0,0,0));
        button.addActionListener(listener);
        frame.add(button);
    }/**
     * Adds a JButton without margins to a given JFrame and assigns it to a give ActionListener
     * Used in this model to assigne large numbers of buttons to the same frame and listener.
     *
     * Takes either a String or an Icon as arguments to display
     * @param button
     * @param text
     * @param frame
     * @param listener
     */
    public static void addButtonToFrame(JButton button, String text, JFrame frame, ActionListener listener) {
        button.setText(text);
        button.setMargin(new Insets(0,0,0,0));
        button.addActionListener(listener);
        frame.add(button);
    }

    /**
     * Creates a dialog box with separate text fields for the entry of multiple pieces of data
     *
     * @param labels Labels for each text box created. Should correspond to information requested
     * @param title Title of the dialog box created
     * @return An ArrayList of text, each corresponding to what was written in the respective text field
     */
    public static ArrayList<String> getInputFromList(String[] labels, String title){
        return (getInputFromList(labels, title, null));
    }    public static ArrayList<String> getInputFromList(String[] labels, String title, String[] currentData){

        Object[] panelContent = new Object[2*labels.length];
        JTextField[] input = new JTextField[labels.length];

        for (int i = 0; i < labels.length; i++) {
            JLabel label = new JLabel();
            label.setText(labels[i]);
            JTextField inputBox = new JTextField();
            input[i] = inputBox;
            if(currentData != null){
                inputBox.setText(currentData[i]);
            }
            panelContent[2*i] = label;
            panelContent[2*i+1] = inputBox;
        }

        int option = JOptionPane.showConfirmDialog(null, panelContent, title, JOptionPane.OK_CANCEL_OPTION);
        if(option == JOptionPane.YES_OPTION) {
            ArrayList<String> output = new ArrayList<>();
            for (int i = 0; i < input.length; i++) {
                output.add(input[i].getText());
            }
            return output;
        } else {
            return null;
        }
    }
}
