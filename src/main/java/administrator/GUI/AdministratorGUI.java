package administrator.GUI;

import administrator.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * This class represents the highest level of the model visible to the user.
 * The UI consists of an array of buttons that serve different functionalities.
 *
 * I have made an effort to make the method names and proceedings as self-explanatory as possible.
 * As the GUI consists of mostly buttons, and the buttons themselves acts as controllers,
 * I have not made a separation between view and controller.
 *
 * In this and the other GUI classesrestricted representations of the equivilant classes in the model is used.
 * The methods seen here mainly call for various text prompts, dialog boxes and drop menus to be presented to the user.
 * Input from the user is then sent to the model-specific classes, where data is processed and changes are made.
 *
 * @Author Lars J. Sverkeli
 */
public class AdministratorGUI extends JPanel implements ActionListener {

    private ISkoleAdministrator model;
    private final JButton backup = new JButton();
    private final JButton openTrinn = new JButton();
    private final JButton openKlasse = new JButton();
    private final JButton openElev = new JButton();
    private final JButton newTrinn = new JButton();
    private final JButton newKlasse = new JButton();
    private final JButton newElev = new JButton();
    private final JButton removeTrinn = new JButton();
    private final JButton removeKlasse = new JButton();
    private final JButton removeElev = new JButton();
    private final JButton addNotat = new JButton();
    private final JButton getNotater = new JButton();
    private final JButton lagEpostliste = new JButton();
    private final JLabel label = new JLabel();


    public AdministratorGUI(ISkoleAdministrator model) {
        this.model = model;
        this.initialize();
    }

    private void initialize() {
        JFrame adminFrame = new JFrame("Klasseboken");
        initializeButtons(adminFrame);
        initializeLabels(adminFrame);
        adminFrame.setSize(1100,340);
        adminFrame.setLayout(null);
        adminFrame.setVisible(true);
        adminFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initializeLabels(JFrame frame) {
        Font font = new Font("SansSerif", Font.BOLD, 30);
        label.setBounds(100,10,400,100);
        label.setFont(font);
        label.setText("Klasseboken");
        frame.add(label);
    }

    private void initializeButtons(JFrame adminFrame) {
        backup.setBounds(10, 10, 80, 20);
        GUIHelpers.addButtonToFrame(backup,"Backup",adminFrame,this);
        openTrinn.setBounds(100, 100, 200, 50);
        GUIHelpers.addButtonToFrame(openTrinn,"Velg trinn",adminFrame,this);
        openKlasse.setBounds(100, 170, 200, 50);
        GUIHelpers.addButtonToFrame(openKlasse,"Velg klasse",adminFrame,this);
        openElev.setBounds(100, 240, 200, 50);
        GUIHelpers.addButtonToFrame(openElev,"Velg elev",adminFrame,this);
        newTrinn.setBounds(350, 100, 200, 50);
        GUIHelpers.addButtonToFrame(newTrinn,"Nytt trinn",adminFrame,this);
        newKlasse.setBounds(350, 170, 200, 50);
        GUIHelpers.addButtonToFrame(newKlasse,"Ny klasse",adminFrame,this);
        newElev.setBounds(350, 240, 200, 50);
        GUIHelpers.addButtonToFrame(newElev,"Ny elev",adminFrame,this);
        removeTrinn.setBounds(600, 100, 200, 50);
        GUIHelpers.addButtonToFrame(removeTrinn,"Fjern eksisterende trinn",adminFrame,this);
        removeKlasse.setBounds(600, 170, 200, 50);
        GUIHelpers.addButtonToFrame(removeKlasse,"Fjern eksisterende klasse",adminFrame,this);
        removeElev.setBounds(600, 240, 200, 50);
        GUIHelpers.addButtonToFrame(removeElev,"Fjern eksisterende elev",adminFrame,this);
        addNotat.setBounds(850, 100, 200, 50);
        GUIHelpers.addButtonToFrame(addNotat,"Skriv notat",adminFrame,this);
        getNotater.setBounds(850, 170, 200, 50);
        GUIHelpers.addButtonToFrame(getNotater,"Se notater",adminFrame,this);
        lagEpostliste.setBounds(850, 240, 200, 50);
        GUIHelpers.addButtonToFrame(lagEpostliste,"Generer epostliste",adminFrame,this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == backup) {
            saveCurrentState();
        } else if (e.getSource() == openTrinn) {
            openExistingTrinn();
        } else if (e.getSource() == openKlasse) {
            openExistingKlasse();
        } else if (e.getSource() == openElev) {
            openExistingElev();
        } else if (e.getSource() == newTrinn) {
            addNewTrinn();
        } else if (e.getSource() == newKlasse) {
            addNewKlasse();
        } else if (e.getSource() == newElev) {
            addNewElev();
        } else if (e.getSource() == removeTrinn) {
            removeTrinn();
        } else if (e.getSource() == removeKlasse) {
            removeKlasse();
        } else if (e.getSource() == removeElev) {
            removeElev();
        } else if (e.getSource() == addNotat) {
            addNotat();
        } else if (e.getSource() == getNotater) {
            getNotater();
        } else if (e.getSource() == lagEpostliste) {
            lagEpostliste();
        }
    }

    private void saveCurrentState() {
        if(model.saveState()) {
            GUIHelpers.simpleDialogBox("Lagring fullført");
        }
    }

    private void openExistingTrinn() {
        String[] trinn = Helpers.asStringArray(model.getTrinnListe());
        int index = GUIHelpers.getDropmenu(trinn, "Hvilket trinn vil du åpne?", "velg trinn", "Finner ingen trinn");
        if (index != -1) {
            TrinnGUI trinnFrame = new TrinnGUI(model.getTrinnListe().get(index));
        }
    }

    private void openExistingKlasse() {
        String[] klasser = Helpers.asStringArray(model.getKlasser());
        int index = GUIHelpers.getDropmenu(klasser, "Hvilken klasse vil du åpne?", "velg Klasse", "Finner ingen klasse");
        if (index != -1) {
            KlasseGUI klasseFrame = new KlasseGUI(model.getKlasser().get(index));
        }
    }

    private void openExistingElev() {
        String[] elever = Helpers.asStringArray(model.getElever());
        int index = GUIHelpers.getDropmenu(elever, "Hvilken elev vil du åpne?", "velg Klasse", "Finner ingen elev");
        if (index != -1) {
            ElevGUI elevFrame = new ElevGUI(model.getElever().get(index));
        }
    }

    private void addNewTrinn() {
        String id = JOptionPane.showInputDialog("Hvilket trinn skal legges til?");
        if (id == null){
            return;
        }
        if (model.checkIfTrinnExist(id)) {
            GUIHelpers.simpleDialogBox("Trinnet finnes allerede");
            return;
        }
        model.addTrinn(id);
        model.sortTrinn();
    }

    private void addNewKlasse() {
        String[] trinnAsString = Helpers.asStringArray(model.getTrinnListe());
        int index = GUIHelpers.getDropmenu(trinnAsString, "Til hvilket trinn skal klassen legges til?", "velg trinn", "Det finnes ingen trinn, kan ikke legge til klasse");
        Trinn trinn;
        if (index != -1) {
            trinn = model.getTrinnListe().get(index);
        } else {
            return;
        }

        AdministrativeGUIHelpers.addNewKlasse(trinn);
    }

    private void addNewElev() {
        String[] klasser = Helpers.asStringArray(model.getKlasser());
        int index = GUIHelpers.getDropmenu(klasser, "Til hvilke klasse skal elever legges til?", "velg klasse", "Det finnes ingen klasse, kan ikke legge til elev");
        Klasse klasse;
        if (index != -1) {
            klasse = model.getKlasser().get(index);
        } else {
            return;
        }
        AdministrativeGUIHelpers.addNewElev(klasse);
    }

    private void removeTrinn() {
        String[] trinn = Helpers.asStringArray(model.getTrinnListe());
        int index = GUIHelpers.getDropmenu(trinn, "Hvilket trinn vil du slette? Alle klasser og elever på trinnet vil også slettes", "velg trinn", "Finner ingen trinn");
        if (index != -1) {
            int result = GUIHelpers.simpleConfirmBox("Vil du virkelig slette " + model.getTrinnListe().get(index).toString() + "?","Slett trinn?");
            if (result == JOptionPane.YES_OPTION) {
                model.removeTrinn(index);
            }
        }
    }

    private void removeKlasse() {
        String[] klasser = Helpers.asStringArray(model.getKlasser());
        int index = GUIHelpers.getDropmenu(klasser, "Hvilken klasse vil du slette? Alle elever i klassen vil også slettes", "velg klasse", "Finner ingen klasse");
        if (index != -1) {
            int result = GUIHelpers.simpleConfirmBox("Vil du virkelig slette " + model.getKlasser().get(index).toString() + "?","Slett klasse?");
            if (result == JOptionPane.YES_OPTION) {
                model.removeKlasse(index);
            }
        }
    }

    private void removeElev() {
        String[] eleverAsStrings = Helpers.asStringArray(model.getElever());
        int index = GUIHelpers.getDropmenu(eleverAsStrings, "Hvilken elev vil du slette?", "velg elev", "finner ingen elev");
        if (index != -1) {
            int result = GUIHelpers.simpleConfirmBox("Vil du virkelig slette " + eleverAsStrings[index] + "?","Slett elev?");
            if (result == JOptionPane.YES_OPTION) {
                model.removeElev(index);
            }
        }
    }

    private void addNotat() {
        String notat = GUIHelpers.getInputFromTextArea("Skriv inn notat");
        if (notat != null) {
            model.addNotat(notat);
        }
    }

    private void getNotater() {
        String notater = Helpers.arrayListAsSingleString(model.getNotater(), "\n");
        GUIHelpers.showTextField(notater, "notater");
    }

    private void lagEpostliste() {
        GUIHelpers.showTextField(model.getEpost(), "epostliste");
    }
}
