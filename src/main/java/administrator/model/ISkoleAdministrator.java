package administrator.model;

import java.util.ArrayList;

/**
 * Interface containing the overarching features of the model, needed to maintain and administrate the hierarchy of the model.
 *
 * @Author Lars J. Sverkeli
 */
public interface ISkoleAdministrator extends IBookkeeper{

    /**
     * Removes a Trinn from the model. All Klasse and Elev associated with the Trinn will also be removed
     * @param index - index of the Trinn in the list of Trinn
     */
    void removeTrinn(int index);

    /**
     * Removes a Klasse from the model
     * @param index index of the Klasse to be removed
     */
    void removeKlasse(int index);

    /**
     * Removes an elev from the model
     *
     * @param index index number of the Elev to be removed
     */
    void removeElev(int index);

    /**
     * Returns the list of Trinn
     * @return list of Trinn
     */
    ArrayList<Trinn> getTrinnListe();

    /**
     * A method to retrieve all Klasse stored in the model.
     * Iterated through all Trinn and retrieves all Klasse-objects.
     *
     * @return An ArrayList containing every Klasse-object in the model, ordered by Trinn
     */
    ArrayList<Klasse> getKlasser();

    /**
     * Method to retrieve all Elev in the model.
     * Iterated through all Trinn and Klasse and adds all Elev to an arrayList
     *
     * @return an ArryList containing all Elev, ordered by Trinn and Klasse
     */
    ArrayList<Elev> getElever();

    /**
     * Attempts to save the current state of the model by serializing to a save file.
     *
     * @return boolean depending on whether save was successful
     */
    boolean saveState();

    /**
     * Adds a new Trinn to the list of Trinn
     * @param nr - the denominator for the Trinn to be generated
     */
    void addTrinn (String nr);

    /**
     * Sorts the list of Trinn ascending based on their denominator.
     */
    void sortTrinn();

    /**
     * Checks whether a Trinn of the given denominator exsist
     *
     * @param id Trinn ID to be checked
     * @return boolean, whether the Trinn exist or not.
     */
    boolean checkIfTrinnExist(String id);
}
