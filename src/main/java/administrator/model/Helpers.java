package administrator.model;


import javax.swing.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class contains helper methods used in this project.
 * Most of these methods are very simple procedures generating a simple output from an input.
 * Some of these methods are probably already in libraries, but if so I wasn't aware of them
 *
 * @author Lars J. Sverkeli
 */
public class Helpers implements Serializable {

    /**
     * Makes a deep copy of a given ArrayList
     *
     * @param arrayList to be copied
     * @param <E> generic type
     * @return a copy of arrayList
     */
    public static <E> ArrayList copyArrayList(ArrayList<E> arrayList) {
        ArrayList<E> copy = new ArrayList<>();
        for (E element : arrayList) {
            copy.add(element);
        }
        return copy;
    }


    /**
     * Method to return the index of a given element in an array
     * @param array array containing the element
     * @param element element to determine index of
     * @return index of element in array. Returns -1 if element not found
     */
    public static int getArrayIndex(Object[] array, Object element) {
        for (int i = 0; i < array.length; i++) {
            if(array[i] == element){
                return i;
            }
        }
        return -1;
    }

    /**
     * Method to get current date as a formatted String
     * @return  Current date as string formatted dd.MM.yy
     */
    public static String getDateAsString(){
        LocalDate date = LocalDate.now();
        String stringDate = date.format(DateTimeFormatter.ofPattern("dd.MM.yy"));
        return stringDate;
    }

    /**
     * Combines an array of Strings to a single string
     *
     * @param stringArray array of String to be combined
     * @param elementSeparator String to be used as separator when joining elements
     * @return Array of strings combined to a single String
     */
    public static String stringArrayToSingleString(String[] stringArray, String elementSeparator){
        String combinedString = "";
        if(stringArray == null){
            return null;
        }
        for (String string : stringArray) {
            combinedString = combinedString + string + elementSeparator;
        }
        return combinedString;
    }

    /**
     * Algorithm for dividing a list of String into groups of a given size.
     *
     * This method takes a list of Elev and returns a string with Elev randomly grouped.
     * Each Elev object has a list of other Elev they should not be grouped with, and this algorithm takes this into consideration.
     * Two incompatible Elev will never be grouped.
     *
     * This algorithm is a brute force algorithm based on randomization and many trials.
     * If groups cannot be made due to restrictions for incompatible Elev, it will try again.
     * If, after 1000 trials, no satisfactory groups can be made, the algorith returns "Kunne ikke lage grupper"
     *
     * @param input ArrayList of Elev to be grouped
     * @param size Size of the groups to be made
     * @return A String containing groups of Elev.toString. Elev are seperated by "\n" while groups are seperated by "\n\n"
     */
    public static ArrayList<ArrayList<IElevReader>> makeElevGroups(ArrayList<IElevReader> input, int size) { // I realize this is a bit of a monster method. i honestly found it hard to break down into smaller pieces.
        return makeElevGroups(input, size, 0);
    }
    private static ArrayList<ArrayList<IElevReader>> makeElevGroups(ArrayList<IElevReader> input, int size, int trials) {
        if(trials > 1000) {
            return null;
        }
        Random rand = new Random();
        ArrayList<ArrayList<IElevReader>> grupper = new ArrayList<>();

        ArrayList<IElevReader> elever = Helpers.copyArrayList(input);

        int attemptsToAddFailed = 0;
        ArrayList<IElevReader> gruppe = new ArrayList<>();
        while(elever.size() > 0){
            int index = rand.nextInt(elever.size());

            boolean isOkToAdd = true;
            for (IElevReader elev : gruppe) {
                for (String negativ : elev.getNegative()) {
                    if(negativ.equals(elever.get(index).toString())) {
                        isOkToAdd = false;
                    }
                }
            }
            if (!isOkToAdd){
                attemptsToAddFailed ++;
                if (attemptsToAddFailed > 10) {
                    return Helpers.makeElevGroups(input, size, trials + 1);
                }
            } else {
                gruppe.add(elever.get(index));
                elever.remove(index);
                attemptsToAddFailed = 0;
            }

            if (elever.size() == 0) {
                grupper.add(gruppe);
            } else if (gruppe.size() == size) {
                grupper.add(gruppe);
                gruppe = new ArrayList<>();
            }
        }
        return grupper;
    }

    public static boolean confirmPassword(String password, int attempts, int failcount) {
        if (failcount == attempts) {
            return false;
        }

        String attempt = JOptionPane.showInputDialog("Skriv inn passord.");
        if (attempt.equals(password)) {
            return true;
        } else {
            return confirmPassword(password, attempts, failcount + 1);
        }
    }


    /**
     * Helper method for generating a condensed email list from a list of Elev.
     *
     * @param elever - list of Elev of which email adresses should be extracted
     * @return A single string containing emails, separated by newlines
     */
    public static String getEpostListe (ArrayList<Elev> elever) {
        ArrayList<String> epostListe = new ArrayList<>();
        for (Elev elev : elever) {
            for (Foresatt foresatt : elev.getForesatte()) {
                if (foresatt.getEpost().equals("")){
                    continue;
                }
                epostListe.add(foresatt.getEpost());
            }
        }
        String epostAsString = Helpers.arrayListAsSingleString(epostListe, ",\n");
        return epostAsString;
    }

    /**
     * Method to parse a string to a date using one of two formats
     *
     * Returns null if the string given cannot be parsed
     * @param date String to be parsed as a date
     * @return LocalDate interpreted from the given String
     */
    public static LocalDate parseDate(String date) {
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("ddMMyyyy");
        try {
            LocalDate parsed = LocalDate.parse(date, formatter1);
            return parsed;
        } catch (Exception e) {}
        try {
            LocalDate parsed = LocalDate.parse(date, formatter2);
            return parsed;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Function to turn an ArrayList of any type to an array of String.
     *
     * This method uses a generic implementation of ArrayList and outputs a String[]
     * Each element in the resulting array corresponds to the equivalent  object in the input.
     * It is recommended that toString() is properly implemented for the input type.
     *
     * @param arrayList to be parsed as an array of strings
     * @return an array of Strings corresponding to the ArrayList given.
     */
    public static <E> String[] asStringArray(ArrayList<E> arrayList) {
        String[] stringArray = new String[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            stringArray[i] = arrayList.get(i).toString();
        }
        return stringArray;
    }

    /**
     * Function to turn an ArrayList of any type to a condensed String.
     *
     * This method uses a generic implementation of ArrayList and outputs a condensed String.
     * The resulting String corresponds to each object in the input seperated by a given String seperator..
     * It is recommended that toString() is properly implemented for the input type.
     *
     * @param arrayList to be parsed as an array of strings.
     * @param separator String argument used to separate elements in arrayList.
     * @return A String containing elements in the array separated by the given separator.
     */
    public static <E> String arrayListAsSingleString(ArrayList<E> arrayList, String separator) {
        String[] stringArray = asStringArray(arrayList);
        return stringArrayToSingleString(stringArray, separator);
    }

    /**
     * A method to return a nested ArrayList 2 deep as a single condensed String
     *
     * Elements are separated by the given separators
     *
     * @param arrayList nested ArrayList, 2 deep
     * @param separator1 String separating inner elements
     * @param separator2 String separating outer elements
     * @param <E> Generic type
     * @return A condensed string of inner elements seperated by separator, seperated by 2*separator
     */
    public static <E> String nestedArrayListAsString(ArrayList<ArrayList<E>> arrayList, String separator1, String separator2) {
        if(arrayList == null) {
            return null;
        }
        String combinedString = "";
        for (ArrayList<E> arrayListE : arrayList){
            for (E element : arrayListE) {
                combinedString = combinedString + element.toString() + separator1;
            }
            combinedString = combinedString + separator2;
        }
        return combinedString;
    }
}
