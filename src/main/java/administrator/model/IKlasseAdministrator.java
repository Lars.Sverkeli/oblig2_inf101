package administrator.model;

import java.util.ArrayList;

/**
 * Interface that contains the core features needed to administrate a Klasse.
 * This interface is used to connect the Klasse in the administrative model with controls/view in the GUI.
 *
 * @Author Lars J. Sverkeli
 */
public interface IKlasseAdministrator extends IBookkeeper, IKlasseInfo {

    /**
     * Returns an ArrayList of restricted Elev corresponding to all Elev held in this object
     *
     * The Elev in this arraylist is restricted by the IElevReader, which limits the objects to retrieving key information about the Elev
     * @return an arraylist of restricted elev
     */
    ArrayList<IElevReader> getReadOnlyElever();

    /**
     * Method to return the last Elev in this object's list of Elev, restricted by the IElevAdministrator interface.
     * This interface allows some modification of the elev. This method is used to add Foresatte to an
     * elev directly after a new Elev is generated
     * @return the last elev, restricted by IElevAdministrator
     */
    IElevAdministrator getLastElev();

    /**
     * Returns a specific elev, restricted by the IElevAdministrator interface.
     * This interface allows some modification of the elev.
     *
     * This method is called when a new ElevGUI element is generated
     *
     * @param index - index of the respective elev in this object's list of Elev
     * @return an Elev, restricted by IElevAdministrator
     */
    IElevAdministrator getIElev(int index);

    /**
     * Method to register attendance for current day.
     *
     * @param registreringer list of attendance
     */
    void registrerTilstede(String dato, ArrayList<String> registreringer);

    /**
     * Outputs the registered Tilstede for all Elev at the given index.
     *
     * @param index - tilstede to be retrieved
     * @return ArrayList of String containnig the registered Stilstede
     */
    ArrayList<String> getSingleTilstede(int index);

    /**
     * Outputs the registered Lekse for all Elev at the given index.
     *
     * @param index - lekse to be retrieved
     * @return ArrayList of String containing the registered respective Lekse for each Elev
     */
    ArrayList<String> getSingleLekse(int index);

    /**
     * Sets the value of a specific tilstede for all Elev in Klasse
     * @param index of the tilstede entry to be edited
     * @param data ArrayList of Strings containing data for each respective Elev
     */
    void setSingleTilstede(int index, ArrayList<String> data);

    /**
     * Registers a new instance of Lekse and adds an empty entry of this Lekse to all Elev contained in this Klasse
     * @param lekse
     */
    void nyLekse(String lekse);

    /**
     * Registers Lekse for all elev
     *
     * @param data ArrayList of String containing data to be registered for each Elev
     * @param index the index of the respectiv Lekse in the list of Lekse
     */
    void registrerLekse(ArrayList<String> data, int index);

    /**
     * Adds an Elev to this Klasse's list of Elev
     *
     * @param elevData - Array containing all data needed to generate a new elev.
     *                 This array need to contain the exact number of variables called for by the constructor of Elev.
     *                 This is ensured by only calling this method through input in a dialog box.
     */
    void addElev(String[] elevData);

    /**
     * Sorts all instances of Elev in this Klasse ascending, according to Fornavn - Etternavn
     */
    void sortElever();

    /**
     * Method to mark two Elev as incompatible
     *
     * An Elev will not be grouped with an incompatible Elev by the automatic group generator.
     *
     * @param elev1 - index - elev to be marked incompatible with elev 2
     * @param elev2 - index - elev to be marked incompatible with elev 1
     */
    void addNegativ(int elev1, int elev2);

    /**
     * Adds a Notat to a specific elev selected from the list of Elev
     *
     * @param index - index of Elev
     * @param notat - String for the Notat to be added to the Elev's list of Notat
     */
    void addElevNotat(int index, String notat);
}
