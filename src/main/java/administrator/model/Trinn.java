package administrator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * A class representing a Trinn, the level above Klasse at a school.
 * Trinn contains a collection of Klasse, which contains a collection of Elev.
 *
 * This class contains simple administrative tools
 *
 * @Author Lars J. Sverkeli
 */
public class Trinn implements Serializable, ITrinnAdministrator {

    private static final long serialVersionUID = 2L;

    private String trinn;
    private ArrayList<Klasse> klasser = new ArrayList<>();
    private ArrayList<Notat> notater = new ArrayList<>();

    /**
     * The constructor for Trinn.
     * This constructor assigns the denominator for the Trinn, usually represented by a single number.
     * This constructor should only be called using the addTrinn method in the Administrator klasse, ensuring the Trinn will be properly assigned
     * This is important for maintaining the hierarchy, and should not be tampered with.
     * @param trinn - String used as an identifier for this specific Klasse
     */
    Trinn (String trinn) {
        this.trinn = trinn;
    }

    /** @inhetitDoc */
    @Override
    public void addKlasse(String tegn) {
        klasser.add(new Klasse(this, tegn));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<IElevReader> getReadOnlyElever(){
        ArrayList<IElevReader> restrictedElever = new ArrayList<>();
        for (Elev elev: getElever()) {
            restrictedElever.add((IElevReader) elev);
        }
        return restrictedElever;
    }


    ArrayList<Elev> getElever() {
        ArrayList<Elev> elever = new ArrayList<>();
        for (Klasse klasse : klasser) {
            for (Elev elev : klasse.getElever()) {
                elever.add(elev);
            }
        }
        return elever;
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<IKlasseInfo> getRestrictedKlasser() {
        ArrayList<IKlasseInfo> restrictedKlasser = new ArrayList<>();
        for (Klasse klasse : klasser) {
            restrictedKlasser.add((IKlasseInfo) klasse);
        }
        return restrictedKlasser;
    }

    /** @inhetitDoc */
    @Override
    public IKlasseAdministrator getKlasseAdministrator(int index) {
        return (IKlasseAdministrator) klasser.get(index);
    }

    ArrayList<Klasse> getKlasser() {
        return klasser;
    }

    @Override
    public String toString() {
        return trinn + ". trinn";
    }

    public void sortKlasser() {
        // The lambda expression used here was copied directly from stack overflow
        Collections.sort(klasser, ((o1, o2) -> o1.getTegn().compareTo(o2.getTegn())));
    }

    String getId() {
        return this.trinn;
    }

    /** @inhetitDoc */
    @Override
    public boolean checkIfKlasseExist(String tegn) {
        tegn = tegn.toUpperCase(Locale.ROOT);
        for (Klasse klasse : klasser) {
            if (tegn.equals(klasse.getTegn())) {
                return true;
            }
        }
        return false;
    }

    /** @inhetitDoc */
    @Override
    public void addNotat(String notat) {
        notater.add(new Notat(notat));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Notat> getNotater() {
        return notater;
    }

    /** @inhetitDoc */
    @Override
    public String getEpost() {
        return Helpers.getEpostListe(getElever());
    }

    void initiateNullPointers () {
        if (klasser == null) {
            klasser = new ArrayList<>();
        }
        if (notater == null) {
            notater = new ArrayList<>();
        }
        for (Klasse klasse : klasser) {
            klasse.initiateNullPointers();
        }
    }
}
