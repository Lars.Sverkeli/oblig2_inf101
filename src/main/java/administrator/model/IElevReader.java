package administrator.model;

import java.util.ArrayList;

/**
 * This interface restricts the functionality of an Elev to viewing key information about an Elev.
 * The main use of this interface is to limit functionality when an Elev is represented in other parts of the GUI
 *
 * @Author Lars J. Sverkeli
 */
public interface IElevReader {

    /**
     * An array of String containing core data of Elev.
     * This corresponds to the data provided when the constructor for Elev is called
     * @return Array containing core Elev data
     */
    String[] getElevData();

    /**
     * @return core information of the elev, with information on Foresatt and incompatible Elev
     */
    String getElevKort();

    /**
     * @return Name of the Elev, "Fornavn Etternavn"
     */
    String getElevNavn();

    /**
     * @return A string containing the name of all Elev considered incompatible with Elev
     */
    String[] getNegative();

    /**
     * @return A list of all Foresatte associated with Elev
     */
    ArrayList<Foresatt> getForesatte();

    /**
     *
     */
    String[] getForesattData(Foresatt foresatt);

    /**
     * @return an array containing the name of all Elev at the same Trinn as Elev
     */
    String[] getTrinnElever();
}
