package administrator.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * This class represents an Elev as a part of a Klasse.
 * An Elev object is used to store key information about the Elev, such as personal information, information about Foresatte,
 * as well as registration of attendance and homework.
 *
 * @Author Lars J. Sverkeli
 */
public class Elev implements Serializable, IElevAdministrator {

    private static final long serialVersionUID = 4L;

    private String fornavn;
    private String etternavn;
    private Klasse klasse;
    private Trinn trinn;
    private LocalDate bursdag;
    private String kommentar;
    private String telefon;

    private ArrayList<Foresatt> foresatte = new ArrayList<>();
    private ArrayList<Elev> negative = new ArrayList<>();
    private ArrayList<Notat> notater = new ArrayList<>();
    private ArrayList<String> tilstedeListe = new ArrayList<>();
    private ArrayList<String> lekser = new ArrayList<>();

    /**
     * Constructor for the Elev class. This constructor assigns core information to the elev, and specifies which Klasse this elev belongs to.
     * This constructor is only called in the addElev method of a Klasse, in order to add a new elev to the Klasse.
     * This ensures that all Elev are always appropriately assigned to a klasse, thus maintaining the hierarchy, and the should not be tampered with
     * @param fornavn fornavn
     * @param etternavn etternavn
     * @param bursdag følselsdag
     * @param telefon telefonnr
     * @param kommentar kommentar - this is shown on ElevKort, and should be used to provide additional, important information about Elev
     * @param klasse the Klasse calling the constructor
     */
    Elev (String fornavn, String etternavn, String bursdag, String telefon, String kommentar, Klasse klasse) {
        this.fornavn = fornavn;
        this.etternavn = etternavn;
        this.klasse = klasse;
        if(bursdag.equals("")){
            this.bursdag = Helpers.parseDate("01011000");
        } else {
            this.bursdag = Helpers.parseDate(bursdag);
        }
        this.telefon = telefon;
        this.kommentar = kommentar;
        this.trinn = klasse.getTrinn();

        // Fyller liste med lekse- og oppmøteregistrering for å unngå konflikt/OutOfBoundsErrors med klassen
        int lekseRegistreringer = klasse.getLekser().size();
        for (int i = 0; i < lekseRegistreringer; i++) {
            lekser.add("-");
        }
        int tilstedeRegistreringer = klasse.getTilstede().size();
        for (int i = 0; i < tilstedeRegistreringer; i++) {
            tilstedeListe.add("-");
        }
    }

    void initiateNullPointers() {
        if (foresatte == null){
            foresatte = new ArrayList<>();
        }
        if (negative == null){
            negative = new ArrayList<>();
        }
        if (notater == null){
            notater = new ArrayList<>();
        }
        if (tilstedeListe == null){
            tilstedeListe = new ArrayList<>();
        }
        if (lekser == null){
            lekser = new ArrayList<>();
        }
    }


    /** @inhetitDoc */
    @Override
    public void setElevData(String[] data) {
        this.fornavn = data[0];
        this.etternavn = data[1];
        if(data[2].equals("")){
            this.bursdag = Helpers.parseDate("01011000");
        } else {
            this.bursdag = Helpers.parseDate(data[2]);
        }
        this.telefon = data[3];
        this.kommentar = data[4];
    }

    /** @inhetitDoc */
    @Override
    public String toString() {
        return fornavn + " " + etternavn + ", " + trinn.getId() + klasse.getTegn();
    }

    /** @inhetitDoc */
    @Override
    public void addNotat(String notat) {
        notater.add(new Notat(notat));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Notat> getNotater (){
        return notater;
    }

    /** @inhetitDoc */
    @Override
    public String getEpost() {
        String epost = "";
        for (Foresatt foresatt : foresatte) {
            epost = epost + foresatt.getEpost() + "\n";
        }
        return epost;
    }

    /** @inhetitDoc */
    @Override
    public String[] getElevData() {
        String[] elevData = {
                fornavn,
                etternavn,
                bursdag.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                telefon,
                kommentar,
        };
        return elevData;
    }

    /** @inhetitDoc */
    @Override
    public String getElevKort() {
        String elevData = fornavn + " " + etternavn + "\n" +
                "Født: " + bursdag.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) + "\n" +
                "Telefon: " + telefon + "\n\n" +
                kommentar + "\n\n";

        String foresatteData = "Foresatte: \n";
        if (foresatte.size() == 0) {
            foresatteData = foresatteData + "Ingen foresatte lagt til";
        }
        for (Foresatt foresatt : foresatte) {
            String[] data = getForesattData(foresatt);
            String fornavn = data[0];
            String etternavn = data[1];
            String telefon = data[2];
            String epost = data[3];
            String adresse = data[4];
            foresatteData = foresatteData + fornavn + " " + etternavn + "\n";
            if (telefon.length() > 0) {
                foresatteData = foresatteData + "Telefon: " + telefon + "\n";
            }
            if (epost.length() > 0) {
                foresatteData = foresatteData + "Epost: " + epost + "\n";
            }
            if (adresse.length() > 0) {
                foresatteData = foresatteData + "Adresse: " + adresse;
            }
            foresatteData = foresatteData + "\n\n";
        }

        String negative = "";
        if (this.negative.size() != 0) {
            negative = negative + "\n\nInkompatible elever:\n";
            for (Elev negElev : this.negative) {
                negative = negative + negElev.toString() + "\n";
            }
        }

        String text = elevData + foresatteData + negative;

        return text;
    }

    /** @inhetitDoc */
    @Override
    public String[] getNegative() {
        return Helpers.asStringArray(negative);
    }

    /** @inhetitDoc */
    @Override
    public void addNegativ(int index) {
        ArrayList<Elev> elever = this.trinn.getElever();
        addNewNegativ(elever.get(index));
    }

    void addNewNegativ(Elev elev){
        for (Elev negativ : this.negative) {
            if (negativ == elev) {
                return;
            }
        }
        negative.add(elev);
        elev.addNewNegativ(this);
    }

    /** @inhetitDoc */
    @Override
    public void negativToRemove(int index) {
        Elev elev = negative.get(index);
        removeNegativ(elev);
    }

    void removeNegativ(Elev elev){
        if (negative.remove(elev)){
            elev.removeNegativ(this);
        }
    }

    /** @inhetitDoc */
    @Override
    public String[] getTrinnElever() {
        return Helpers.asStringArray(trinn.getElever());
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Foresatt> getForesatte() {
        return foresatte;
    }

    /** @inhetitDoc */
    @Override
    public String[] getForesattData(Foresatt foresatt) {
        String[] foresattData = {
                foresatt.getForNavn(),
                foresatt.getEtterNavn(),
                foresatt.getTelefon(),
                foresatt.getEpost(),
                foresatt.getAdresse()
        };
        return foresattData;
    }

    /** @inhetitDoc */
    @Override
    public void setForesattData(Foresatt foresatt, String[] data) {
        foresatt.setForNavn(data[0]);
        foresatt.setEtterNavn(data[1]);
        foresatt.setTelefon(data[2]);
        foresatt.setEpost(data[3]);
        foresatt.setAdresse(data[4]);
    }

    /** @inhetitDoc */
    @Override
    public void addForesatt(String[] data) {
        Foresatt foresatt = new Foresatt(data[0], data[1], data[2], data[3],data[4], this);
        foresatte.add(foresatt);
    }

    /** @inhetitDoc */
    @Override
    public void removeForesatt(int index) {
        if (index >= foresatte.size()) {
            return;
        }
        foresatte.remove(index);
    }

    /** @inhetitDoc */
    @Override
    public String getElevNavn(){
        return fornavn + " " + etternavn;
    }


    Klasse getKlasse(){
        return klasse;
    }

    Trinn getTrinn() {
        return trinn;
    }

    void addLekse(String lekse){
        lekser.add(0, lekse);
    }

    void setLekse(int id, String lekse){
        lekser.set(id, lekse);
    }

    void setTilstede(int id, String tilstedeReg){
        tilstedeListe.set(id, tilstedeReg);
    }

    ArrayList<String> getLekser(){
        return lekser;
    }

    void addToTilstede(String registrering) {
        tilstedeListe.add(0, registrering);
    }


    ArrayList<String> getTilstedeListe() {
        return tilstedeListe;
    }


    String getTilstede(int index){
        return tilstedeListe.get(index);
    }


    String getLekse(int index){
        return lekser.get(index);
    }

}
