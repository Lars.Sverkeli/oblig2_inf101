package administrator.model;

/**
 * Interface that contains the core features needed to administrate Elev.
 * This interface is used to connect the Elev in the administrative model with controls/view in the GUI.
 *
 * @Author Lars J. Sverkeli
 */
public interface IElevAdministrator extends IBookkeeper, IElevReader {

    /**
     * Edits a Foresatt in this Elev's list of Foresatt
     *
     * @param data - Array containing all data needed to edit Foresatt.
     *                 This array need to contain the exact number of variables, equivalent to calling the constructor of Foresatt.
     *                 This is ensured by only calling this method through input in a dialog box.
     */
    void setForesattData(Foresatt foresatt, String[] data);

    /**
     * Adds a Foresatt to this Elev's list of Foresatt
     *
     * @param data - Array containing all data needed to generate a new Foresatt.
     *                 This array need to contain the exact number of variables called for by the constructor of Foresatt.
     *                 This is ensured by only calling this method through input in a dialog box.
     */
    void addForesatt(String[] data);

    /**
     * removes a Foresatt to this Elev's list of Foresatt
     *
     * @param index - index of foresatt to be removed
     */
    void removeForesatt(int index);

    /**
     * Method specifying the index of an elev to be removed from the list of incompatible Elev
     * @param index of the elev to be removed
     */
    void negativToRemove(int index);

    /**
     * Edits core information about Elev.
     *
     * @param elevData - Array containing the core data for Elev, used to edit this information.
     *                 This array need to contain an exact number of variables, equal to calling the constructor of Elev.
     *                 This is ensured by only calling this method through input in a dialog box.
     */
    void setElevData(String[] elevData);

    /**
     * Method to add an incompatible elev to Elev
     * @param index of the elev to be added, from a list of elev
     */
    void addNegativ(int index);

}
