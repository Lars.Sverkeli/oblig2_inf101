package administrator.model;

import java.util.ArrayList;

/**
 * This interface connects the Trinn in the administrative model with controls and view of the GUI
 *
 * @Author Lars J. Sverkeli
 */
public interface ITrinnAdministrator extends IBookkeeper {

    /**
     * Method to return a restricted version of all Elev objects stored in this object.
     * The Elev returned is restricted by the IElevReader interface
     *
     * @return and ArrayList of restricted Elev
     */
    ArrayList<IElevReader> getReadOnlyElever();

    /**
    * Checks whether a Klasse of the given denominator exsist
    *
    * @param tegn - Klasse ID to be checked
    * @return boolean, whether the Klasse exist or not.
    */
    boolean checkIfKlasseExist(String tegn);

    /**
     * Method to sort all Klasse present in the object, ascending according to ther denominator
     */
    void sortKlasser();

    /**
     * Method to add a Klasse with the given doenominator to this object's list of Klasse
     * @param tegn
     */
    void addKlasse(String tegn);

    /**
     * Method to return a restricted version of all Klasse objects stored in this object.
     * The Klasse returned is restricted by the IKlasseAdministrator interface
     *
     * @return and ArrayList of restricted Elev
     */
    ArrayList<IKlasseInfo> getRestrictedKlasser();

    /**
     * Method to return a specific restricted Klasse from this Object's klasser
     *
     * This Klasse is restricted by the IKlasseAdministrator interface. This interface extends IKlasseInfo, and is thus less restrictive.
     *
     * @param index of the Klasse to be returned
     * @return An instance of Klasse, restricted by the IKlasseAdministrator interface.
     */
    IKlasseAdministrator getKlasseAdministrator(int index);

}
