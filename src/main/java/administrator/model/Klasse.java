
package administrator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * A class representing a Klasse, in the school sense of the word.
 * Klasse contains a list of Elev, and several administrative tools for managing Elev
 *
 * The key functions are tools to register and view records of homework and attendance,
 * but other simple tools, as well as an automatic group maker is also included.
 *
 * The group maker lets the user make randomly assigned groups of desired size, and will not group Elev assigned as incompatible
 *
 * @Author Lars J. Sverkeli
 */
public class Klasse implements Serializable, IKlasseAdministrator {

    private static final long serialVersionUID = 3L;

    private Trinn trinn;
    private String tegn;
    private ArrayList<Elev> elever = new ArrayList<>();
    private ArrayList<Notat> notater = new ArrayList<>();
    private ArrayList<String> tilstede = new ArrayList<>();
    private ArrayList<String> lekser = new ArrayList<>();

    /**
     * The constructor for Klasse.
     * This constructor assigns the denominator for the classe, usually represented by a single letter.
     * This constructor should only be called using the addKlasse method in the Trinn klasse, important for maintaining the hierarchy.
     * This ensures the Klasse will be properly assigned to a Trinn, and should not be tampered with.
     * @param trinn the Trinn calling the constructor
     * @param tegn used as an identifier for this specific Klasse
     */
    Klasse (Trinn trinn, String tegn) {
        this.trinn = trinn;
        this.tegn = tegn.toUpperCase(Locale.ROOT);
    }

    void initiateNullPointers() {
        if (elever == null) {
            elever = new ArrayList<>();
        }
        if (notater == null) {
            notater = new ArrayList<>();
        }
        if (tilstede == null) {
            tilstede = new ArrayList<>();
        }
        if (lekser == null) {
            lekser = new ArrayList<>();
        }
        for (Elev elev : elever) {
            elev.initiateNullPointers();
        }
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<String>getTilstede(){
        return tilstede;
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<String> getSingleTilstede(int index){
        ArrayList singleTilstede = new ArrayList();
        for (Elev elev : elever) {
            singleTilstede.add(elev.getTilstede(index));
        }
        return singleTilstede;
    }

    /** @inhetitDoc */
    @Override
    public void setSingleTilstede(int index, ArrayList<String> data){
        for (int i = 0; i < data.size(); i++) {
            elever.get(i).setTilstede(index,data.get(i));
        }
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<ArrayList<String>> getTilstedeOversikt() {
        ArrayList<ArrayList<String>> tilstedeListe = new ArrayList<>();
        for (Elev elev : elever) {
            tilstedeListe.add(elev.getTilstedeListe());
        }
        return tilstedeListe;
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<ArrayList<String>> getLekseListe() {
        ArrayList<ArrayList<String>> lekseListe = new ArrayList<>();
        for (Elev elev : elever) {
            lekseListe.add(elev.getLekser());
        }
        return lekseListe;
    }

    /** @inhetitDoc */
    @Override
    public void nyLekse(String lekse) {
        lekser.add(0, lekse);
        for (Elev elev : elever) {
            elev.addLekse("");
        }
    }

    /** @inhetitDoc */
    @Override
    public void registrerLekse(ArrayList<String> data, int index) {
        for (int i = 0; i < data.size(); i++) {
            elever.get(i).setLekse(index, data.get(i));
        }
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<String> getLekser(){
        return lekser;
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<String> getSingleLekse(int index){
        ArrayList singleLekse = new ArrayList();
        for (Elev elev : elever) {
            singleLekse.add(elev.getLekse(index));
        }
        return singleLekse;
    }

    /** @inhetitDoc */
    @Override
    public void registrerTilstede(String dato, ArrayList<String> registreringer) {

        for (int i = 0; i < registreringer.size(); i++) {
            String data = registreringer.get(i);
            if (data.equals("")) {
                data = "ok";
            }
            elever.get(i).addToTilstede(data);
        }
        tilstede.add(0, dato);
    }

    /** @inhetitDoc */
    @Override
    public void addElev(String[] elevData) {
        String fornavn = elevData[0];
        String etternavn = elevData[1];
        String bursdag = elevData[2];
        String telefon = elevData[3];
        String kommentar = elevData[4];
        elever.add(new Elev(fornavn, etternavn, bursdag, telefon, kommentar, this));
    }

    ArrayList<Elev> getElever() {
        return elever;
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<String> getElevNavn() {
        ArrayList<String> elevNavn = new ArrayList<>();
        for (Elev elev : elever) {
            elevNavn.add(elev.getElevNavn());
        }
        return elevNavn;
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<IElevReader> getReadOnlyElever() {
        ArrayList<IElevReader> restriktiveElever = new ArrayList<>();
        for (Elev elev : elever) {
            restriktiveElever.add((IElevReader) elev);
        }
        return restriktiveElever;
    }

    Elev getElev (int nr) {
        return elever.get(nr);
    }


    /** @inhetitDoc */
    @Override
    public IElevAdministrator getLastElev() {
        return (IElevAdministrator) getElev(getElever().size() - 1);
    }


    /** @inhetitDoc */
    @Override
    public IElevAdministrator getIElev(int index) {
        return (IElevAdministrator) elever.get(index);
    }


    /** @inhetitDoc */
    @Override
    public void sortElever() {
        // The lambda expression used here was copied directly from stack overflow
        Collections.sort(elever, ((o1, o2) -> o1.getElevNavn().compareTo(o2.getElevNavn())));
    }

    /** @inhetitDoc */
    @Override
    public void addNegativ(int elev1, int elev2){
        elever.get(elev1).addNewNegativ(elever.get(elev2));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Notat> getElevNotat(int index) {
        return elever.get(index).getNotater();
    }

    /** @inhetitDoc */
    @Override
    public void addElevNotat(int index, String notat) {
        elever.get(index).addNotat(notat);
    }

    /** @inhetitDoc */
    @Override
    public void addNotat(String notat) {
        notater.add(new Notat(notat));
    }


    /** @inhetitDoc */
    @Override
    public ArrayList<Notat> getNotater () {
        return notater;
    }


    /** @inhetitDoc */
    @Override
    public String getEpost() {
        return Helpers.getEpostListe(getElever());
    }


    String getTegn() {
        return tegn;
    }

    Trinn getTrinn() {
        return trinn;
    }

    /** @inhetitDoc */
    @Override
    public String toString() {
        return "Klasse " + this.trinn.getId() + tegn;
    }
}
