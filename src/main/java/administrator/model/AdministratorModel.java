package administrator.model;

import administrator.IO;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This model represents a school, and is intended as an administrative tool for teachers.
 * Terminology used is generally Norwegian.
 *
 * The model has functionality for adding layers of information in a hierarchy.
 *
 * The top layer contains a list of Trinn, which contains a list of Klasse, which contains a list of Elev.
 * Each layer has functonality appropriate for the given level. For instance, on the Klasse level, there is functionality
 * for generating groups of Elev, while avoiding the grouping of incompatible Elev.
 *
 * The model contains some tools bookkeeping, including immutable notes at all levels,
 * and functionality for registering homework og attendance for Elev.
 *
 * Constructors of Trinn, Klasse and Elev are all set to package private and is only called from dedicated "add" methods from the above class.
 * This ensures the hierarchy is maintained, and should not be tampered with
 *
 * @Author Lars J. Sverkeli
 */
public class AdministratorModel implements Serializable, ISkoleAdministrator {

    private static final long serialVersionUID = 1L;

    private ArrayList<Trinn> trinnListe = new ArrayList<>();
    private ArrayList<Notat> notater = new ArrayList<>();


    /**
     * This method is called wheneved the model is opened, and will initialize all null pointers.
     * This method attempts to provide backwards compatibility for old save files with new versions of the program,
     * by avoiding null-pointers from occuring due to variables being added after a save file was stored.
     *
     * This method has not been properly tested, apart from the fact that it won't break anything in the current version.
     */
    public void initiateNullPointers() {
        if (trinnListe == null) {
            trinnListe = new ArrayList<>();
        }
        if (notater == null) {
            notater = new ArrayList<>();
        }
        for (Trinn trinn: trinnListe) {
            trinn.initiateNullPointers();
        }
    }

    /** @inhetitDoc */
    @Override
    public boolean saveState() {
        try {
            IO.writeModelToDisk(this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return false;
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
            return false;
        }
        return true;
    }

    /** @inhetitDoc */
    @Override
    public void addTrinn (String nr){
        trinnListe.add(new Trinn(nr));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Trinn> getTrinnListe() {
        return trinnListe;
    }

    /** @inhetitDoc */
    @Override
    public void removeTrinn(int nr){
        trinnListe.remove(nr);
    }

    /** @inhetitDoc */
    @Override
    public void sortTrinn() {
        // The lambda expression used here was copied directly from stack overflow
        Collections.sort(trinnListe, ((o1, o2) -> o1.getId().compareTo(o2.getId())));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Klasse> getKlasser() {
        ArrayList<Klasse> klasser = new ArrayList<>();
        for (Trinn trinn : trinnListe) {
            for (Klasse klasse : trinn.getKlasser()) {
                klasser.add(klasse);
            }
        }
        return klasser;
    }

    /** @inhetitDoc */
    @Override
    public void removeKlasse(int nr) {
        Klasse klasse = getKlasser().get(nr);
        Trinn trinn = klasse.getTrinn();
        trinn.getKlasser().remove(klasse);
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Elev> getElever() {
        ArrayList<Elev> elever = new ArrayList<>();
        for (Trinn trinn : trinnListe) {
            for (Klasse klasse : trinn.getKlasser()) {
                for (Elev elev : klasse.getElever()) {
                    elever.add(elev);
                }
            }
        }
        return elever;
    }

    /** @inhetitDoc */
    @Override
    public void removeElev(int nr) {
        Elev elev = getElever().get(nr);
        Klasse klasse = elev.getKlasse();
        klasse.getElever().remove(elev);
    }

    /** @inhetitDoc */
    @Override
    public boolean checkIfTrinnExist(String id) {
        for (Trinn trinn : trinnListe) {
            if (id.equals(trinn.getId())) {
                return true;
            }
        }
        return false;
    }

    /** @inhetitDoc */
    @Override
    public void addNotat(String notat) {
        notater.add(new Notat(notat));
    }

    /** @inhetitDoc */
    @Override
    public ArrayList<Notat> getNotater() {
        return notater;
    }

    /** @inhetitDoc */
    @Override
    public String getEpost() {
        return Helpers.getEpostListe(getElever());
    }
}
