package administrator.model;

import java.util.ArrayList;
/**
 * This interface restricts the functionality of a Klasse to viewing key information about the Klasse.
 * The main use of this interface is to limit functionality when a Klasse is represented in other parts of the GUI
 *
 * @Author Lars J. Sverkeli
 */
public interface IKlasseInfo {

    /**
     * Retrieves every lekse registered on Klasse
     * @return ArrayList of Strings representing Lekse
     */
    ArrayList<String> getLekser();

    /**
     * @return an ArrayList containing the name of each Elev in klasse
     */
    ArrayList<String> getElevNavn();

    /**
     * @return a nested ArrayList containing data corresponding to all registered tilstede for all Elev in Klasse.
     */
    ArrayList<ArrayList<String>> getTilstedeOversikt();

    /**
     * @return an overview of all instances of Tilstede registered for the Klasse
     */
    ArrayList<String> getTilstede();

    /**
     * @return a nested ArrayList containing data corresponding to all registered lekse for all Elev in Klasse.
     */
    ArrayList<ArrayList<String>> getLekseListe();

    /**
     * Retrieves all Notat for the specified Elev
     * @param index index of Elev in the list of ELev
     * @return An array list containing all Notat for the selected Elev
     */
    ArrayList<Notat> getElevNotat(int index);

}
