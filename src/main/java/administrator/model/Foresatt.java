package administrator.model;

import java.io.Serializable;

/**
 * This class is used to bundle important personal information of a caretaker to a single object.
 * All foresatt should be associated with an Elev.
 *
 * @Author Lars J. Sverkeli
 */
public class Foresatt implements Serializable {

    private static final long serialVersionUID = 6L;

    private String forNavn;
    private String etterNavn;
    private String epost;
    private String telefon;
    private String adresse;
    private final Elev elev;

    /**
     * Constructor for the Foresatt class.
     * This constructor should only be called in conjunction with adding a Foresatt to an Elev
     * @param forNavn
     * @param etterNavn
     * @param telefon
     * @param epost
     * @param adresse
     * @param elev - should be the elev calling the constructor
     */
    Foresatt(String forNavn, String etterNavn, String telefon, String epost, String adresse, Elev elev) {
        this.forNavn = forNavn;
        this.etterNavn = etterNavn;
        this.epost = epost;
        this.adresse = adresse;
        this.telefon = telefon;
        this.elev = elev;
    }

    String getForNavn() {
        return forNavn;
    }

    String getEtterNavn() {
        return etterNavn;
    }

    String getEpost() {
        return epost;
    }

    String getTelefon() {
        return telefon;
    }

    String getAdresse() {
        return adresse;
    }

    void setForNavn(String forNavn) {
        this.forNavn = forNavn;
    }

    void setEtterNavn(String etterNavn) {
        this.etterNavn = etterNavn;
    }

    void setEpost(String epost) {
        this.epost = epost;
    }

    void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return forNavn + " " + etterNavn + ", foresatt for " + elev.toString();
    }
}
