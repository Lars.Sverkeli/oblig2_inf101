package administrator.model;

import java.util.ArrayList;

/**
 * This interface contains simple methods for bookkeeping, and is extended
 * by the interfaces corresponding to each level representing the hierarchy of the model
 *
 * @Author Lars J. Sverkeli
 */
public interface IBookkeeper {

    /**
     * Adds an immutable note
     * Notes are automatically time stamped upon creation, with the format "Dato: dd-MM-yyyy hh.mm"
     * @param notat String to be added to the note
     */
    void addNotat(String notat);

    /**
     * @return a list of all Notat
     */
    ArrayList<Notat> getNotater();

    /**
     * Generates a mailing list with the email adresses for all foresatte of all elev at the current level
     * @return mailing list as a single String
     */
    String getEpost();

}
