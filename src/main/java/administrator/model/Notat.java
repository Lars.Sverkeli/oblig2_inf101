package administrator.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * This class represents a timestamped note
 *
 * The class is immutable - once the note is created, its content or time stamp cannot be altered.
 * The time stamp is generated whenever a new note is created.
 * @author Lars J. Sverkeli
 */
public class Notat implements Serializable {

    private static final long serialVersionUID = 6L;

    public final LocalDateTime dato;
    public final String notat;

    /**
     * Cunstructor for the Notat class
     * @param notat - string used to represent the content of the note.
     */
    Notat(String notat) {
        this.dato = LocalDateTime.now();
        this.notat = notat;
    }

    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH.mm");
        return "Dato: " + dato.format(format) +
                "\n" + notat + '\n';
    }
}
