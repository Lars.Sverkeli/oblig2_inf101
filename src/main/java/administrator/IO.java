package administrator;

import administrator.model.AdministratorModel;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IO {

    /**
     * Retrieves a backup file of the model serialized. Allows saving the state of the program.
     *
     * @return model
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws FileNotFoundException
     */
    public static AdministratorModel getStoredModel() throws IOException, ClassNotFoundException, FileNotFoundException {
        AdministratorModel model = new AdministratorModel();
        FileInputStream fileInputStream = new FileInputStream("data.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        model = (AdministratorModel) objectInputStream.readObject();
        objectInputStream.close();
        return model;
    }

    /**
     * Writes the current state of the model as a serialized object to a file stored on disk
     *
     * @param model - object to be serialized
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void writeModelToDisk(AdministratorModel model) throws IOException, ClassNotFoundException {
        try{
            moveToBackup();
        } catch(Exception e){
            e.printStackTrace();
        }
        FileOutputStream fileOutputStream = new FileOutputStream("data.ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(model);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    /**
     * Moves the current save file to a backup folder. Called before storing current state in writeModelToDisk.
     * Allows saving of backups to restore previous state of the model.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws FileNotFoundException
     */
    public static void moveToBackup() throws IOException, ClassNotFoundException, FileNotFoundException {
        LocalDateTime current = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss");

        String currentPath = new java.io.File(".").getPath();
        Files.move(Paths.get( currentPath + "/data.ser"),Paths.get(currentPath + "/backup/" + current.format(format) + ".ser"));
    }
}
