package administrator;

import administrator.GUI.GUIHelpers;
import administrator.model.AdministratorModel;
import administrator.GUI.AdministratorGUI;
import administrator.model.Helpers;

import javax.swing.*;
import java.io.*;

/**
 * This is an administrativ tool for teachers teaching in "barneskole"
 * The main method tries to initialize the model by deserializeing from a serialized save file
 * All null pointers are then initiated, and a GUI element calls the model.
 *
 * For more information on the program, see Javadoc and readme.md
 *
 * @Author Lars J. Sverkeli
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        AdministratorModel model = new AdministratorModel();

        try {
            model = IO.getStoredModel();
            model.initiateNullPointers();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // This is not meant to be a secure password, but just an obstacle for any random person to be able to open the program.
        // This is commented out for now.
/*
        if (Helpers.confirmPassword("SamplePass", 3, 0)){
            JPanel view = new AdministratorGUI(model);
        } else {
            GUIHelpers.simpleDialogBox("For mange forsøk, programmet avsluttes.");
            System.exit(0);
        }
 */

        JPanel view = new AdministratorGUI(model);

        AdministratorModel finalModel = model;

        // Autosave whenever program shuts down
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                try {
                    IO.writeModelToDisk(finalModel);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }, "Shutdown-thread"));
    }
}
